const mix = require('laravel-mix');
require('laravel-mix-mjml');
let LiveReloadPlugin = require('webpack-livereload-plugin');
mix.disableNotifications()

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/index.js', 'public/js')
    .js('resources/js/auth/login.js', 'public/js/auth')
    .js('resources/js/auth/register.js', 'public/js/auth')
    .js('resources/js/auth/password/email.js', 'public/js/auth/password')
    .sass('resources/sass/app.scss', 'public/css')
    .mjml();

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});
