/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/auth/register.js":
/*!***************************************!*\
  !*** ./resources/js/auth/register.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function submitFormRegister() {
  var spinnerClass = 'spinner spinner-right spinner-white pr-15';

  var formFunction = function formFunction() {
    var form = KTUtil.getById('formRegister');
    var url = KTUtil.attr(form, 'action');
    var btn = KTUtil.getById('submitBtnRegisterForm');

    if (!form) {
      return;
    }

    FormValidation.formValidation(form, {
      fields: {
        name: {
          validators: {
            notEmpty: {
              message: 'Votre nom est requise'
            }
          }
        },
        firstname: {
          validators: {
            notEmpty: {
              message: 'Votre prénom est requise'
            }
          }
        },
        email: {
          validators: {
            notEmpty: {
              message: 'Votre adresse mail est requise'
            }
          }
        },
        password: {
          validators: {
            notEmpty: {
              message: 'Votre mot de passe est requis'
            }
          }
        },
        password_confirmation: {
          validators: {
            notEmpty: {
              message: 'Votre mot de passe est requis'
            },
            different: {
              compare: function compare() {
                return form.querySelector('#password').value;
              },
              message: "Le mot de passe et sa confirmation sont différent."
            }
          }
        }
      },
      plugins: {
        trigger: new FormValidation.plugins.Trigger(),
        submitButton: new FormValidation.plugins.SubmitButton(),
        //defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
        bootstrap: new FormValidation.plugins.Bootstrap({//	eleInvalidClass: '', // Repace with uncomment to hide bootstrap validation icons
          //	eleValidClass: '',   // Repace with uncomment to hide bootstrap validation icons
        })
      }
    }).on('core.form.valid', function (e) {
      e.preventDefault();
      KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter...');
      FormValidation.utils.fetch(url, {
        method: 'POST',
        dataType: 'json',
        params: {
          name: form.querySelector('#name').value,
          firstname: form.querySelector('#firstname').value,
          email: form.querySelector('#email').value,
          password: form.querySelector('#password').value,
          newsletter: form.querySelector('#newsletter').value
        }
      }).then(function (response) {
        KTUtil.btnRelease(btn);

        if (response && _typeof(response) === "object" && response.status && response.status === 'success') {
          window.location.href = '/';
        } else {
          swal.fire({
            text: 'Oh Oh! Une erreur à eu lieu, veuillez réessayer plus tard !',
            icon: 'error',
            buttonsStyling: false,
            confirmButtonText: "Ok, pas de problème!",
            customClass: {
              confirmButton: "btn font-weight-bold btn-light-primary"
            }
          }).then(function () {
            KTUtil.scrollTop();
          });
        }
      });
    }).on('core.form.invalid', function () {
      Swal.fire({
        text: "Oh Oh !! Plusieurs erreurs ont eu lieu. Veuillez réessayer plus tard",
        icon: "error",
        buttonsStyling: false,
        confirmButtonText: "Ok, pas de problème!",
        customClass: {
          confirmButton: "btn font-weight-bold btn-light-primary"
        }
      }).then(function () {
        KTUtil.scrollTop();
      });
    });
  };
}

jQuery(document).ready(function () {
  submitFormRegister();
});

/***/ }),

/***/ 3:
/*!*********************************************!*\
  !*** multi ./resources/js/auth/register.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\LOGICIEL\laragon\www\probioairsante\resources\js\auth\register.js */"./resources/js/auth/register.js");


/***/ })

/******/ });