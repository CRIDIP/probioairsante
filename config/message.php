<?php

return [
    'newsletter' => "Je désire recevoir votre lettre d'information",
    'accept' => "J'accepte les termes et conditions de la politique de confidentialité.",
    'accountupdated' => 'Vos informations ont bien été mises à jour.',
    'rgpd' => "Vous avez la possibilité d'accéder à vos informations personnelles mémorisées dans notre base de données. Vous avez juste à cliquer sur le bouton ci-dessous pour télécharger un document PDF comportant la totalité de vos données.",
    'addresssaved' => "L'adresse a bien été enregistrée.",
    'addressupdated' => "L'adresse a bien été modifiée.",
    'addressdeleted' => "L'adresse a bien été supprimée.",
    'rangesupdated' => 'Les plages ont bien été mises à jour. Il est conseillé de mettre éventuellement à jour les tarifs d\'expédition.',
    'colissimosupdated' => 'Les tarifs Colissimo ont bien été mis à jour.',
];
