<?php

use App\Models\{Colissimo, Country, PaymentType, Product, ProductCategory, Range, Shop, State};
use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            ['name' => 'France', 'tax' => 0.2],
            ['name' => 'Belgique', 'tax' => 0.2],
            ['name' => 'Suisse', 'tax' => 0],
            ['name' => 'Canada', 'tax' => 0],
        ]);

        Range::insert([
            ['max' => 1],
            ['max' => 2],
            ['max' => 5],
            ['max' => 10],
            ['max' => 30],
        ]);

        Colissimo::insert([
            ['country_id' => 1, 'range_id' => 1, 'price' => 7.95],
            ['country_id' => 1, 'range_id' => 2, 'price' => 8.95],
            ['country_id' => 1, 'range_id' => 3, 'price' => 13.75],
            ['country_id' => 1, 'range_id' => 4, 'price' => 20.05],
            ['country_id' => 1, 'range_id' => 5, 'price' => 28.55],
            ['country_id' => 2, 'range_id' => 1, 'price' => 15.50],
            ['country_id' => 2, 'range_id' => 2, 'price' => 17.55],
            ['country_id' => 2, 'range_id' => 3, 'price' => 22.45],
            ['country_id' => 2, 'range_id' => 4, 'price' => 37.00],
            ['country_id' => 2, 'range_id' => 5, 'price' => 61.50],
        ]);

        State::insert([
            ['name' => 'Attente chèque', 'slug' => 'cheque', 'color' => 'primary', 'indice' => 1],
            ['name' => 'Attente mandat administratif', 'slug' => 'mandat', 'color' => 'primary', 'indice' => 1],
            ['name' => 'Attente virement', 'slug' => 'virement', 'color' => 'primary', 'indice' => 1],
            ['name' => 'Attente paiement par carte', 'slug' => 'carte', 'color' => 'primary', 'indice' => 1],
            ['name' => 'Erreur de paiement', 'slug' => 'erreur', 'color' => 'danger', 'indice' => 0],
            ['name' => 'Annulé', 'slug' => 'annule', 'color' => 'danger', 'indice' => 2],
            ['name' => 'Mandat administratif reçu', 'slug' => 'mandat_ok', 'color' => 'success', 'indice' => 3],
            ['name' => 'Paiement accepté', 'slug' => 'paiement_ok', 'color' => 'success', 'indice' => 4],
            ['name' => 'Expédié', 'slug' => 'expedie', 'color' => 'success', 'indice' => 5],
            ['name' => 'Remboursé', 'slug' => 'rembourse', 'color' => 'danger', 'indice' => 6],
        ]);

        ProductCategory::insert([
            ["name" => "PBRS"],
            ["name" => "Protections & Hygiènes"],
        ]);

        Product::insert([
            ["category_id" => 1, "name" => "PBRS 1000", "price" => "1044.00", "weight" => "1.70", "active" => 1, "quantity" => 50, "quantity_alert" => 10, "description" => "description"],
            ["category_id" => 1, "name" => "Recharche pour PBRS 1000", "price" => "142.50", "weight" => "0.177", "active" => 1, "quantity" => 50, "quantity_alert" => 10, "description" => "description"],
            ["category_id" => 1, "name" => "PBRS 500", "price" => "4314.50", "weight" => "0.177", "active" => 1, "quantity" => 50, "quantity_alert" => 10, "description" => "description"],
        ]);

        factory(Shop::class)->create();

        PaymentType::insert([
            [
                "name" => "Carte Bancaire",
                "dolibarr_payment_id" => 6
            ],
            [
                "name" => "Chèque Bancaire",
                "dolibarr_payment_id" => 7
            ],
            [
                "name" => "Mandat Administratif",
                "dolibarr_payment_id" => 54
            ],
            [
                "name" => "Virement Bancaire",
                "dolibarr_payment_id" => 2
            ],
        ]);
    }
}
