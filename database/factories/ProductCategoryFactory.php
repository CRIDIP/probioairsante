<?php

/** @var Factory $factory */

use App\Models;
use App\Models\ProductCategory;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ProductCategory::class, function (Faker $faker) {
    return [
        "name" => $faker->sentence(3)
    ];
});
