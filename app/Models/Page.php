<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'title', 'slug', 'text', 'menu',
    ];
    public $timestamps = false;

    public static function PageAsMenu() {
        return self::where('menu', 1)->get();
    }

    public static function PageAsNotMenu() {
        return self::where('menu', 0)->get();
    }
}
