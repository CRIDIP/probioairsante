<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConnexion extends Model
{
    protected $fillable = ["user_id", "ip", "created_at", "updated_at"];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
