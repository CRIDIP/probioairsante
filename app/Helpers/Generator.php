<?php


namespace App\Helpers;


use Illuminate\Support\Str;

class Generator
{
    public static function firsLetter(string $word, $length = 1)
    {
        if ($length == 1) {
            return Str::limit($word, 1, null);
        } else {
            return Str::limit($word, $length, null);
        }
    }

    /**
     * Permet de formater un nombre floatant en devise locale
     *
     * @param float $value
     * @param string $currency
     * @return string
     */
    public static function formatCurrency($value, $currency = "euro")
    {
        switch ($currency) {
            case "euro":
                return number_format($value, 2, ',', ' ') . " €";
            case "dollard":
                return number_format($value, 2, ',', ' ') . " $";
        }
    }
}
