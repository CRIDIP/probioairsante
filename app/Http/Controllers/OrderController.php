<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Country;
use App\Models\Product;
use App\Models\State;
use App\Models\User;
use App\Notifications\NewOrder;
use App\Services\Shipping;
use Illuminate\Http\Request;
use Cart;
use Illuminate\Support\Str;

class OrderController extends Controller
{
    public function create(Request $request, Shipping $shipping)
    {
        $addresses = $request->user()->addresses()->get();

        if($addresses->isEmpty()) {
            return redirect()->route('account.adresses.create')->with('message', 'Vous devez créer au moins une adresse pour passer une commande.');
        }

        $country_id = $addresses->first()->country_id;

        $shipping = $shipping->compute($country_id);

        $content = Cart::getContent();

        $total = Cart::getTotal();

        $tax = Country::find($country_id)->tax;

        return view('command.index', compact('addresses', 'shipping', 'content', 'total', 'tax'));
    }

    public function store(Request $request, Shipping $ship)
    {
        // Vérification de stock
        $items = Cart::getContent();
        foreach ($items as $item) {
            $product = Product::findOrFail($item->id);
            if($product->quantity < $item->quantity) {
                return response()->json("Nous sommes désolé mais le produit ".$item->name." ne dispose pas d'un stock nécessaire pour satisfaire votre demande.<br>Il ne reste plus que ".$product->quantity." exemplaires disponibles", 202);
            }
        }

        $user = $request->user();

        $address_facturation = Address::with('country')->findOrFail($request->facturation);
        $address_livraison = $request->different ? Address::with('country')->findOrFail($request->livraison) : $address_facturation;

        $shipping = $request->expedition === 'colissimo' ? $ship->compute($address_livraison->country->id) : 0;

        $tvaBase = Country::whereName('France')->first()->tax;

        $tax = $request->expedition === 'colissimo' ? $address_livraison->country->tax : $tvaBase;

        // Enregistrement commande
        $order = $user->orders()->create([
            'reference' => strtoupper(Str::random(8)),
            'shipping' => $shipping,
            'tax' => $tax,
            'total' => $tax > 0 ? Cart::getTotal() : Cart::getTotal() / (1 + $tvaBase),
            'payment' => $request->payment,
            'pick' => $request->expedition === 'retrait',
            'state_id' => State::whereSlug($request->payment)->first()->id,
        ]);
        // Enregistrement adresse de facturation
        $order->adresses()->create($address_facturation->toArray());
        // Enregistrement éventuel adresse de livraison
        if($request->different) {
            $address_livraison->facturation = false;
            $order->adresses()->create($address_livraison->toArray());
        }
        // Enregistrement des produits
        foreach($items as $row) {
            $order->products()->create(
                [
                    'name' => $row->name,
                    'total_price_gross' => ($tax > 0 ? $row->price : $row->price / (1 + $tvaBase)) * $row->quantity,
                    'quantity' => $row->quantity,
                ]
            );
            // Mise à jour du stock
            $product = Product::findOrFail($row->id);
            $product->quantity -= $row->quantity;
            $product->save();
            // Alerte stock
            if($product->quantity <= $product->quantity_alert) {
                // Notifications à prévoir pour les administrateurs
            }
        }
        // On vide le panier
        Cart::clear();
        Cart::session($request->user())->clear();
        // Notifications à prévoir pour les administrateurs et l'utilisateur
        $admins = User::whereAdmin(true)->get();
        foreach ($admins as $admin) {
            $admin->notify(new NewOrder($order));
        }

        return response()->json($order->id, 201);
    }
}
