<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\Registered;
use App\Models\Shop;
use App\Notifications\NewUser;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Services\Dolibarr\ThirdPartie;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * @var Shop
     */
    private $shop;
    /**
     * @var User
     */
    private $user;
    /**
     * @var ThirdPartie
     */
    private $thirdPartie;

    /**
     * Create a new controller instance.
     *
     * @param Shop $shop
     * @param User $user
     * @param ThirdPartie $thirdPartie
     */
    public function __construct(Shop $shop, User $user, ThirdPartie $thirdPartie)
    {
        $this->middleware('guest');
        $this->shop = $shop->newQuery();
        $this->user = $user->newQuery();
        $this->thirdPartie = $thirdPartie;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'firstname' => $data['firstname'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'newsletter' => array_key_exists('newsletter', $data),
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param Request $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $shop = $this->shop->firstOrFail();
        Mail::to($user)->send(new Registered($shop));

        $client = $this->thirdPartie->createClient($user->name, $user->firstname, $user->email);
        if(!isset($client['error'])) {
            $user->dolibarr_id = $client;
            $user->save();
        }else{
            return back()->with("error", "Impossible de définir l'identifiant dans la base de donnée. ".$client["error"]);
        }


        $admins = $this->user->whereAdmin(true)->get();
        foreach ($admins as $admin) {
            // Notification administrateur
            $admin->notify(new NewUser());
        }

        return redirect(route('account.adresses.create'))->with('info', config('message.registered'));
    }
}
