<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAddress;
use App\Models\Address;
use App\Models\Country;
use Illuminate\Http\Request;

class AddressController extends Controller
{
    /**
     * @var Country
     */
    private $country;
    /**
     * @var Address
     */
    private $address;

    /**
     * AddressController constructor.
     * @param Country $country
     * @param Address $address
     */
    public function __construct(Country $country, Address $address)
    {
        $this->country = $country->newQuery();
        $this->address = $address->newQuery();
    }

    public function index(Request $request)
    {
        $addresses = $request->user()->addresses()->with('country')->get();
        if($addresses->isEmpty()) {
            return redirect(route('account.adresses.create'))->with('message', config('messages.oneaddress'));
        }
        return view('account.addresses.index', compact('addresses'));
    }

    public function create()
    {
        $countries = $this->country->get();

        return view('account.addresses.create', compact('countries'));
    }

    public function store(StoreAddress $storeAddress)
    {
        $storeAddress->merge(['professionnal' => $storeAddress->has('professionnal')]);
        $storeAddress->user()->addresses()->create($storeAddress->all());
        return redirect(route('account.adresses.index'))->with('success', config('message.addresssaved'));
    }

    public function edit($adress_id)
    {
        //$this->authorize('manage', $adress);
        $adress = $this->address->findOrFail($adress_id);
        $countries = Country::all();
        return view('account.adresses.edit', compact('adress', 'countries'));
    }

    public function update(StoreAddress $storeAddress, $adress_id)
    {
        $storeAddress->merge(['professionnal' => $storeAddress->has('professionnal')]);
        $adress = $this->address->findOrFail($adress_id);
        $adress->update($storeAddress->all());
        return redirect(route('account.adresses.index'))->with('success', config('message.addressupdated'));
    }

    public function destroy($adress_id)
    {
        $adress = $this->address->findOrFail($adress_id);
        $adress->delete();

        return back()->with('success', config('message.addressdeleted'));
    }
}
