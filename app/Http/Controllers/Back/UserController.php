<?php

namespace App\Http\Controllers\Back;

use App\DataTables\UsersDataTable;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var User
     */
    private $user;

    /**
     * UserController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user->newQuery();
    }

    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Liste des clients"
        ]);
    }

    public function show($user_id)
    {
        $user = $this->user->with('orders', 'orders.state', 'addresses', 'connexions')->findOrFail($user_id);
        $countCommandeValid = Order::where('user_id', $user_id)->where('state_id', 8)->count();
        $countCommandeInvalid = Order::where('user_id', $user_id)->where('state_id', '!=', 8)->count();
        $sumCommandValid = Order::where('user_id', $user_id)->where('state_id', 8)->sum('total');
        $sumCommandInvalid = Order::where('user_id', $user_id)->where('state_id', '!=', 8)->sum('total');

        return view("back.client.customers.show", compact('user', 'countCommandeValid', 'countCommandeInvalid', 'sumCommandValid', 'sumCommandInvalid'));
    }
}
