<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Range;
use Illuminate\Http\Request;

class RangeController extends Controller
{
    /**
     * @var Range
     */
    private $range;
    /**
     * @var Country
     */
    private $country;

    /**
     * RangeController constructor.
     * @param Range $range
     * @param Country $country
     */
    public function __construct(Range $range, Country $country)
    {
        $this->range = $range->newQuery();
        $this->country = $country->newQuery();
    }

    public function edit()
    {
        $ranges = $this->range->get();

        return view("back.expedition.range.edit", compact('ranges'));
    }

    public function update(Request $request)
    {
        $data = $request->except('_method', '_token');
        $ranges = $this->range->get();

        // Traitement des éventuelles plages supprimées
        $diff = $ranges->count() - count($data);
        if ($diff > 0) {
            $index = $diff;
            while ($index--) {
                $this->range->latest('id')->first()->delete();
            }
        }

        // Mise à jour des valeurs des plages existantes
        $index = 1;
        foreach($ranges as $range) {
            $range->max = $data[$index++];
            $range->save();
        }

        // Ajout éventuel de plages
        if($diff < 0) {
            $index = $diff;
            $countries = $this->country->get();
            while($index++) {
                $range = $this->range->create(['max' => $data[count($data) + $index]]);
                // Affectations par défaut aux pays
                foreach($countries as $country) {
                    $range->countries()->attach($country, ['price' => 0]);
                }
            }
        }

        return back()->with('success', config('message.rangesupdated'));
    }
}
