<?php

namespace App\Http\Controllers\Back;

use App\DataTables\PagesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\PageRequest;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * @var Page
     */
    private $page;

    /**
     * PageController constructor.
     * @param Page $page
     */
    public function __construct(Page $page)
    {
        $this->page = $page->newQuery();
    }

    public function index(PagesDataTable $dataTable)
    {
        return $dataTable->render("back.shared.index", [
            "nameStack" => "Gestion des pages"
        ]);
    }

    public function create()
    {
        return view("back.config.page.create");
    }

    public function store(PageRequest $request)
    {
        $page = $this->page->create($request->all());
        return response()->json("La Page <strong>".$page->title."</strong> à été ajouté avec succès", 200);
    }

    public function edit($page_id)
    {
        $page = $this->page->findOrFail($page_id);

        return view('back.config.page.edit', compact('page'));
    }

    public function update(PageRequest $request, $page_id)
    {
        $page = $this->page->findOrFail($page_id);
        $page->update($request->all());

        return response()->json("La Page <strong>".$page->title."</strong> à été modifié avec succès", 200);
    }

    public function alert($page_id)
    {
        $page = $this->page->findOrFail($page_id);

        return view("back.config.page.alert", compact('page'));
    }

    public function destroy($page_id)
    {
        $page = $this->page->findOrFail($page_id);
        $page->delete();

        return response()->json(null, 200);
    }
}
