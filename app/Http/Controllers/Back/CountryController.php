<?php

namespace App\Http\Controllers\Back;

use App\DataTables\CountriesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\CountryRequest;
use App\Models\Country;
use App\Models\Range;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    /**
     * @var Country
     */
    private $country;
    /**
     * @var Range
     */
    private $range;

    /**
     * CountryController constructor.
     * @param Country $country
     * @param Range $range
     */
    public function __construct(Country $country, Range $range)
    {
        $this->country = $country->newQuery();
        $this->range = $range->newQuery();
    }

    public function index(CountriesDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Gestion des pays"
        ]);
    }

    public function create()
    {
        return view("back.config.pays.create");
    }

    public function store(CountryRequest $request)
    {
        $country = $this->country->create($request->all());

        $ranges = $this->range->get();
        foreach ($ranges as $range) {
            $range->countries()->attach($country, ['price' => 0]);
        }

        return response()->json("Le pays à été ajouté avec succès", 200);
    }

    public function edit(Country $pays)
    {
        return view("back.config.pays.edit", ["country" => $pays]);
    }

    public function update(CountryRequest $request, $country_id)
    {
        $country = $this->country->findOrFail($country_id);
        $country->update($request->all());
        return response()->json("Le pays <strong>".$country->name."</strong> à été mis à jours", 200);
    }

    public function destroy($country_id)
    {
        $country = $this->country->findOrFail($country_id);
        $country->delete();

        return response()->json(null, 200);
    }

    public function alert($country_id)
    {
        $country = $this->country->findOrFail($country_id);
        return view("back.config.pays.alert", compact('country'));
    }
}
