<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Http\Requests\ShopRequest;
use App\Models\Shop;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * @var Shop
     */
    private $shop;

    /**
     * ShopController constructor.
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        $this->shop = $shop->newQuery();
    }

    public function edit()
    {
        $shop = $this->shop->firstOrFail();

        return view('back.config.shop.edit', compact('shop'));
    }

    public function update(ShopRequest $request)
    {
        $request->merge([
            'invoice' => $request->has('invoice'),
            'card' => $request->has('card'),
            'transfer' => $request->has('transfer'),
            'check' => $request->has('check'),
            'mandat' => $request->has('mandat'),
        ]);

        try {
            $this->shop->firstOrFail()->update($request->all());
            return response()->json("La boutique à bien été mise à jour", 200);
        }catch (\Exception $exception) {
            return response()->json($exception->getMessage(), 500);
        }
    }
}
