<?php

namespace App\Http\Controllers\Back;

use App\DataTables\ProductCategoriesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCategoryRequest;
use App\Models\ProductCategory;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProductCategoryController extends Controller
{
    /**
     * @var ProductCategory
     */
    private $category;

    /**
     * ProductCategoryController constructor.
     * @param ProductCategory $category
     */
    public function __construct(ProductCategory $category)
    {
        $this->category = $category->newQuery();
    }

    /**
     * Display a listing of the resource.
     *
     * @param ProductCategoriesDataTable $dataTable
     * @return Response
     */
    public function index(ProductCategoriesDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Gestion des catégories de produit"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view("back.catalog.category.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ProductCategoryRequest $request
     * @return JsonResponse
     */
    public function store(ProductCategoryRequest $request)
    {
        $category = $this->category->create($request->all());

        return response()->json("La catégorie <strong>".$category->name."</strong> à été créer avec succès", 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $category_id
     * @return Factory|View
     */
    public function edit($category_id)
    {
        $category = $this->category->findOrFail($category_id);

        return view("back.catalog.category.edit", compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ProductCategoryRequest $request
     * @param $category_id
     * @return JsonResponse
     */
    public function update(ProductCategoryRequest $request, $category_id)
    {
        $category = $this->category->findOrFail($category_id);
        $category->update($request->all());

        return response()->json("La catégorie <strong>".$category->name."</strong> à été modifier avec succès");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $category_id
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy($category_id)
    {
        $category = $this->category->findOrFail($category_id);
        $category->delete();

        return response()->json(null, 200);
    }

    public function alert($category_id)
    {
        $category = $this->category->findOrFail($category_id);

        return view("back.catalog.category.alert", compact('category'));
    }
}
