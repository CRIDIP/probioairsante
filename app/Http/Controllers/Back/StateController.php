<?php

namespace App\Http\Controllers\Back;

use App\DataTables\StatesDataTable;
use App\Http\Controllers\Controller;
use App\Http\Requests\StateRequest;
use App\Models\State;
use Illuminate\Http\Request;

class StateController extends Controller
{
    /**
     * @var State
     */
    private $state;

    /**
     * StateController constructor.
     * @param State $state
     */
    public function __construct(State $state)
    {
        $this->state = $state->newQuery();
    }

    public function index(StatesDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Gestion des états de commandes"
        ]);
    }

    public function create()
    {
        return view("back.config.etats.create");
    }

    public function store(StateRequest $request)
    {
        $state = $this->state->create($request->all());

        return response()->json("L'état <strong>".$state->name."</strong> à été ajouté avec succès", 200);
    }

    public function edit($state_id)
    {
        $state = $this->state->findOrFail($state_id);

        return view("back.config.etats.edit", compact('state'));
    }

    public function update(StateRequest $request, $state_id)
    {
        $state = $this->state->findOrFail($state_id);
        $state->update($request->all());

        return response()->json("L'état <strong>".$state->name."</strong> à été mise à jours", 200);
    }

    public function alert($state_id)
    {
        $state = $this->state->findOrFail($state_id);
        return view("back.config.etats.alert", compact('state'));
    }

    public function destroy($state_id)
    {
        $state = $this->state->findOrFail($state_id);
        $state->delete();

        return response()->json(null, 200);
    }
}
