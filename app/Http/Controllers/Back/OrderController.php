<?php

namespace App\Http\Controllers\Back;

use App\DataTables\OrdersDataTable;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\State;
use App\Services\Dolibarr\Invoice;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var State
     */
    private $state;

    /**
     * OrderController constructor.
     * @param Order $order
     * @param State $state
     */
    public function __construct(Order $order, State $state)
    {
        $this->order = $order->newQuery();
        $this->state = $state->newQuery();
    }

    public function index(OrdersDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Gestion des commandes"
        ]);
    }

    public function show($id)
    {
        $order = Order::with('adresses', 'products', 'state', 'user', 'user.orders', 'payment_infos')->findOrFail($id);

        $annule_indice = $this->state->whereSlug('annule')->first()->indice; // 2
        if ($order->payment === 'mandat' && !$order->purchase_order) {
            $states = $this->state->where('indice', '<=', $annule_indice)
                ->where('indice', '>', 0)
                ->get();
            //dd("state 1 ".$states);
        } else {
            $states = $this->state->where('indice', '>', $order->state->indice)->get();
           // dd("State 2 ".$states, $order->state->indice);
        }

        return view('back.order.show', compact('order', 'states', 'annule_indice'));

    }

    public function update(Request $request, $order_id)
    {
        $commande = $this->order->findOrFail($order_id)->load('state');
        $states = $this->state->get();

        if ($request->state_id !== $commande->state_id) {
            $indice_payment = $states->firstWhere('slug', 'cheque')->indice;
            $state_new = $states->firstWhere('id', $request->state_id);
            if ($commande->state->indice === $indice_payment && $state_new->indice === $indice_payment) {
                $commande->payment = $states->firstWhere('id', $request->state_id)->slug;
            }

            $commande->state_id = $request->state_id;
            $commande->save();
        }

        return back()->with("success", "L'état de la commande à été mis à jour");
    }

    public function invoice(Request $request, $order_id, Invoice $invoice)
    {
        $order = $this->order->with('products', 'payment')->findOrFail($order_id);
        $response = $invoice->createInvoice($request->user()->dolibarr_id, $order->products);
        if (!isset($response["error"])) {
            $payment = $invoice->createPayment($response, $order->payment->payment_id, $order->total + $order->shipping);
            if (!isset($payment["error"])) {
                return back()->with("success", "La facture à été générer");
            }
        }
    }

    public function updateNumber(Request $request, $order_id)
    {
        $request->validate([
            "purchase_order" => "required|string|max:100"
        ]);
        $commande = $this->order->findOrFail($order_id);
        $commande->purchase_order = $request->purchase_order;
        $commande->save();

        return back()->with("success", "Le numéro du bon de commande à été mis à jour");
    }
}
