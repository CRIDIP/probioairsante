<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Colissimo;
use App\Models\Country;
use App\Models\Range;
use Illuminate\Http\Request;

class ColissimoController extends Controller
{
    /**
     * @var Country
     */
    private $country;
    /**
     * @var Range
     */
    private $range;
    /**
     * @var Colissimo
     */
    private $colissimo;

    /**
     * ColissimoController constructor.
     * @param Country $country
     * @param Range $range
     * @param Colissimo $colissimo
     */
    public function __construct(Country $country, Range $range, Colissimo $colissimo)
    {
        $this->country = $country->newQuery();
        $this->range = $range->newQuery();
        $this->colissimo = $colissimo->newQuery();
    }

    public function edit()
    {
        $countries = $this->country->with('ranges')->get();
        $ranges = $this->range->get();

        return view("back.expedition.colissimo.edit", compact('countries', 'ranges'));
    }

    public function update(Request $request)
    {
        $data = $request->except('_method', '_token');

        // Validation
        $dataValidation = $data;
        foreach($dataValidation as $key => &$value ) {
            $value = 'required|numeric';
        }
        $request->validate($dataValidation);

        $colissimos = $this->colissimo->get();

        foreach($colissimos as $colissimo) {
            $price = $data['n' . $colissimo->id];
            if($colissimo->price !== $price) {
                $colissimo->price = $price;
                $colissimo->save();
            }
        }

        return back()->with('success', config('message.colissimosupdated'));
    }
}
