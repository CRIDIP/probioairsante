<?php

namespace App\Http\Controllers\Back;

use App\DataTables\ProductsDataTable;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use File;


class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var ProductCategory
     */
    private $category;

    /**
     * ProductController constructor.
     * @param Product $product
     * @param ProductCategory $category
     */
    public function __construct(Product $product, ProductCategory $category)
    {
        $this->product = $product->newQuery();
        $this->category = $category->newQuery();
    }

    /**
     * Display a listing of the resource.
     *
     * @param ProductsDataTable $dataTable
     * @return Response
     */
    public function index(ProductsDataTable $dataTable)
    {
        return $dataTable->render('back.shared.index', [
            "nameStack" => "Gestion des produits"
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = $this->category->get();
        return view('back.catalog.product.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $inputs = $this->getInputs($request);
        $product = $this->product->create($inputs);

        return redirect()->back()->with('success', "Le produit <strong>".$product->name."</strong> à été créer avec succès");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $product_id
     * @return Factory|View
     */
    public function edit($product_id)
    {
        $categories = $this->category->get();
        $product = $this->product->with('category')->findOrFail($product_id);

        return view('back.catalog.product.edit', compact('categories', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $product_id
     * @return RedirectResponse
     */
    public function update(Request $request, $product_id)
    {
        $product = $this->product->findOrFail($product_id);
        $inputs = $this->getInputs($request);
        if($request->has('image')) {
            $this->deleteImages($product);
        }
        $product->update($inputs);

        return redirect()->back()->with('success', "Le produit <strong>".$product->name."</strong> à été modifier avec succès");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $product_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($product_id)
    {
        $product = $this->product->findOrFail($product_id);
        $this->deleteImages($product);
        $product->delete();

        return \response()->json(null, 200);
    }

    public function alert($product_id)
    {
        $product = $this->product->findOrFail($product_id);
        return view("back.catalog.product.alert", compact('product'));
    }

    protected function saveImage($request)
    {
        $image = $request->file('image');
        $name = time().'.'.$image->extension();
        $img = Image::make($image->path());
        $img->widen(800)->encode()->save(public_path('/images/').$name);
        $img->widen(400)->encode()->save(public_path('/images/thumbs/').$name);

        return $name;
    }

    protected function getInputs($request)
    {
        $inputs = $request->except(['image']);
        $inputs['active'] = $request->has('active');
        if($request->image) {
            $inputs['image'] = $this->saveImage($request);
        }
        return $inputs;
    }

    protected function deleteImages($produit)
    {
        File::delete([
            public_path('/images/').$produit->image,
            public_path('/images/thumbs/').$produit->image,
        ]);
    }
}
