<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * @var Product
     */
    private $product;
    /**
     * @var ProductCategory
     */
    private $category;

    /**
     * ProductController constructor.
     * @param Product $product
     * @param ProductCategory $category
     */
    public function __construct(Product $product, ProductCategory $category)
    {
        $this->product = $product->newQuery();
        $this->category = $category->newQuery();
    }

    public function index()
    {
        $products = $this->product->get();
        $categories = $this->category->get();

        return view('produits.index', compact('products', 'categories'));
    }

    public function category($category_id)
    {
        $products = $this->product->where('category_id', $category_id)->get();
        $categories = $this->category->get();
        $category = $this->category->find($category_id);

        return view('produits.category', compact('products', 'categories', 'category'));
    }

    public function show(Request $request, $category_id, $product_id) {
        $product = $this->product->find($product_id);
        if($product->active == 1 || $request->user()->admin) {
            return view("produits.show", compact('product'));
        }

        return redirect()->back();
    }
}
