<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\State;
use Illuminate\Http\Request;
use Stripe\Stripe;

class PaymentController extends Controller
{
    public function payment(Request $request, Order $order)
    {
        $this->authorize('manage', $order);
        $state = null;

        if($request->payment_intent_id === 'error') {
            $state = 'erreur';
        } else {
            Stripe::setApiKey(config('stripe.secret_key'));
            $intent = \Stripe\PaymentIntent::retrieve($request->payment_intent_id);
            if ($intent->status === 'succeeded') {
                $request->session()->forget($order->reference);
                $order->payment_infos()->create(['payment_id' => $intent->id]);
                $state = 'paiement_ok';
                // Création de la facture à prévoir
            } else {
                $state = 'erreur';
            }
        }

        $order->state_id = State::whereSlug($state)->first()->id;
        $order->save();
    }
}
