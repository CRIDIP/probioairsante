<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AccountController extends Controller
{
    public function __construct()
    {
    }

    public function index(Request $request)
    {
        $orders = $request->user()->orders()->count();

        return view('account.index', compact('orders'));
    }
}
