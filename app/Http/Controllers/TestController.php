<?php

namespace App\Http\Controllers;

use App\Services\Dolibarr\Invoice;
use App\Services\Dolibarr\Products;
use App\Services\Dolibarr\ThirdPartie;
use Illuminate\Http\Request;
use Cart;

class TestController extends Controller
{
    public function test(ThirdPartie $partie, Products $products, Invoice $invoice)
    {
        dd($invoice->getInvoice(2));
    }
}
