<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Shop;
use Illuminate\Http\Request;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;
use Stripe\Stripe;

class OrdersController extends Controller
{
    /**
     * @var Order
     */
    private $order;
    /**
     * @var Shop
     */
    private $shop;

    /**
     * OrdersController constructor.
     * @param Order $order
     * @param Shop $shop
     */
    public function __construct(Order $order, Shop $shop)
    {
        $this->order = $order->newQuery();
        $this->shop = $shop->newQuery();
    }

    public function index(Request $request)
    {
        $orders = $request->user()->orders()->with('state')->get();
        return view('account.orders.index', compact('orders'));
    }

    public function show(Request $request, $id)
    {
        $order = $this->order->with('products', 'state', 'adresses', 'adresses.country')->findOrFail($id);
        $data = $this->data($request, $order);
        return view('account.orders.show', $data);
    }

    public function confirmation(Request $request, $id)
    {
        $order = $this->order->with('products', 'adresses', 'state')->findOrFail($id);

        if (in_array($order->state->slug, ['cheque', 'mandat', 'virement', 'carte', 'erreur'])) {
            $this->authorize('manage', $order);
            $data = $this->data($request, $order);

            //dd($data);
            return view('command.confirmation', $data);
        }

        return abort(500, 'Erreur Système');
    }

    protected function stripe($data, $request, $order)
    {
        if($request->session()->has($order->reference)) {
            $data['secret'] = $request->session()->get($order->reference);
        } else {
            \Stripe\Stripe::setApiKey(config('stripe.secret_key'));
            try {
                $intent = \Stripe\PaymentIntent::create([
                    'amount' => (integer)($order->totalOrder * 100),
                    'currency' => 'EUR',
                    'metadata' => [
                        'reference' => $order->reference,
                    ],
                ]);
            } catch (ApiErrorException $e) {
                dd($e->getMessage());
            }
            $request->session()->put($order->reference, $intent->client_secret);
            $data['secret'] =  $intent->client_secret;
        };

        return $data;
    }

    private function data($request, $order)
    {
        $shop = $this->shop->firstOrFail();
        $data = compact('order', 'shop');

        if ($order->state->slug == 'carte' || $order->state->slug == 'erreur') {
            $data = $this->stripe($data, $request, $order);
        }

        return $data;
    }
}
