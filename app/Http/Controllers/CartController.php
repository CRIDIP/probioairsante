<?php

namespace App\Http\Controllers;

use App\Models\Product;


use Cart;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @var Product
     */
    private $product;

    /**
     * CartController constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product->newQuery();
    }

    public function index()
    {
        $carts = Cart::getContent();

        return view('cart.index', compact('carts'));
    }

    public function store(Request $request)
    {
        $product = $this->product->findOrFail($request->id);

        $cart = Cart::add([
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price,
                'quantity' => $request->quantity,
                'attributes' => [],
                'associatedModel' => $product,
            ]
        );


        return redirect()->back()->with('cart', 'ok');
    }

    public function update(Request $request, $id)
    {
        Cart::update($id, [
            'quantity' => ['relative' => false, 'value' => $request->quantity]
        ]);

        return redirect(route('panier.index'));
    }

    public function destroy($id)
    {
        try {
            Cart::remove($id);
        }catch (\Exception $exception) {
            dd($exception->getMessage());
        }
        return redirect(route('panier.index'));
    }
}
