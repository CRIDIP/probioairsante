<?php


namespace App\Services\Dolibarr;


use Illuminate\Config\Repository;

class Dolibarr
{
    /**
     * @var Repository
     */
    private $apikey;
    private $headers;
    /**
     * @var string
     */
    private $endpoint;

    public function __construct()
    {
        $this->apikey = config('dolibarr.api_key');
        $this->headers = [
            'DOLAPIKEY: '.$this->apikey,
            'Content-Type:application/json'
        ];
        $this->endpoint = "https://gestion.probioairsante.com/api/index.php/";
    }

    protected function CallAPI($method, $url, $data = false)
    {
        $curl = curl_init();
        $httpheader = ['DOLAPIKEY: '.$this->apikey];

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                $httpheader[] = "Content-Type:application/json";

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

                break;
            case "PUT":

                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
                $httpheader[] = "Content-Type:application/json";

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // Optional Authentication:
        //    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //    curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        curl_setopt($curl, CURLOPT_URL, $this->endpoint.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $httpheader);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}
