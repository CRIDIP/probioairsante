<?php


namespace App\Services\Dolibarr;


class ThirdPartie extends Dolibarr
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getClients()
    {
        return json_decode($this->CallAPI("GET", "thirdparties", [
            "limit" => 10000,
            "sortfield" => "rowid"
        ]), true);

    }
    public function getClientInfo($id)
    {
        return json_decode($this->CallAPI("GET", "thirdparties/".$id), true);
    }

    public function createClient($name, $firstname, $email)
    {
        $params = [
            "name" => $name . ' ' . $firstname,
            "email" => $email,
            "client" => "1",
            "code_client" => "-1"
        ];
        if ($this->isExist($name) == false) {
            $result = json_decode($this->CallAPI("POST", "thirdparties", json_encode($params)), true);
            if (isset($result['error'])) {
                return $result['error']['message'];
            } else {
                return $result;
            }
        } else {
            return $this->searchClient($name);
        }
    }

    public function searchClient($name)
    {
        $clientSearch = json_decode($this->CallAPI("GET", "thirdparties", [
            "sortfield" => "t.rowid",
            "sortorder" => "ASC",
            "limit" => "1",
            "mode" => "1",
            "sqlfilters" => "(t.nom:=:'" . $name . "')"
        ]), true);

        if (isset($clientSearch['error'])) {
                return $clientSearch['error'];
        } else {
            return $clientSearch[0]['entity'];
        }
    }

    protected function isExist($name)
    {
        $clientSearch = json_decode($this->CallAPI("GET", "thirdparties", [
            "sortfield" => "t.rowid",
            "sortorder" => "ASC",
            "limit" => "1",
            "mode" => "1",
            "sqlfilters" => "(t.nom:=:'" . $name . "')"
        ]), true);

        if (isset($clientSearch['error'])) {
            return false;
        } else {
            return true;
        }
    }
}
