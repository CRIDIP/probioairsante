<?php


namespace App\Services\Dolibarr;


class Products extends Dolibarr
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listProducts()
    {
        $params = ["limit" => 10000, "sortfield" => "rowid"];
        $result = $this->CallAPI("GET", 'products', $params);
        $result = json_decode($result, true);

        return $result;
    }

    public function createProduct($reference, $label)
    {
        $params = [
            "ref" => $reference,
            "label" => $label,
            "tosell" => "1"
        ];
        $products = $this->listProducts();
        $searchKey = array_search($label, $products);
        if($searchKey){
            return json_encode(["error" => "Le produit existe déja dans la base dolibarr"]);
        }else{
            $result = json_decode($this->CallAPI("POST", 'products', json_encode($params)), true);

            if(isset($result["error"]) && $result["error"]["code"] >= "300"){
                return json_encode(["error" => $result]);
            }else{
                return $result;
            }
        }
    }

    public function productIsExist($name)
    {
        $productSearch = json_decode($this->CallAPI("GET", "products", [
            "sortfield" => "t.rowid",
            "sortorder" => "ASC",
            "limit" => "1",
            "mode" => "1",
            "sqlfilters" => "(t.label:=:'" . $name . "')"
        ]), true);

        if (isset($productSearch['error'])) {
            return false;
        } else {
            return $productSearch[0]['entity'];
        }
    }
}
