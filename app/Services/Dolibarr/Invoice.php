<?php


namespace App\Services\Dolibarr;


class Invoice extends Dolibarr
{
    /**
     * @var Products
     */
    private $products;

    /**
     * Invoice constructor.
     * @param Products $products
     */
    public function __construct(Products $products)
    {
        parent::__construct();
        $this->products = $products;
    }

    public function listInvoices()
    {
        $params = ["limit" => 10000, "sortfield" => "rowid"];
        $result = $this->CallAPI("GET", 'invoices', $params);
        $result = json_decode($result, true);

       return $result;
    }

    public function createInvoice($client_id, $orderProducts)
    {
        $invoiceLine = [];
        foreach ($orderProducts as $product) {
            $existProduct = $this->products->productIsExist($product->name);
            if($existProduct !== false) {
                $productId = $existProduct;
            }else{
                $productId = $this->products->createProduct('-1', $product->name);
            }
            $invoiceLine[] = [
                "desc" => $product->name,
                "subprice" => $product->total_price_gross / $product->quantity,
                "qty" => $product->quantity,
                "fk_product" => $productId
            ];
        }

        if(count($invoiceLine) > 0) {
            return json_decode($this->CallAPI("POST", "invoices", [
                "socid" => $client_id,
                "type" => "0",
                "lines" => $invoiceLine,
                "note_private" => "Facture automatiquement importer depuis le site web !"
            ]), true);
        }else{
            return json_decode("{error: 'Facture non générer car aucune ligne de la commande n\'est définie.'}");
        }
    }

    public function getInvoice($invoice_id)
    {
        $invoice = [];
        $invoice["invoice"] = json_decode($this->CallAPI("GET", "invoices/".$invoice_id));
        $invoice['lines'] = json_decode($this->CallAPI("GET", "invoices/".$invoice_id."/lines"));
        $invoice['payments'] = json_decode($this->CallAPI("GET", "invoices/".$invoice_id."/payments"));

        return $invoice;
    }

    public function validateInvoice($invoice_id)
    {
        return json_decode($this->CallAPI("POST", "invoices/".$invoice_id."/validate", [
            "idwarehouse" => 0,
            "notrigger" => 0
        ]), true);
    }

    public function createPayment($invoice_id, $mode_dolibarr_id, $amount)
    {
        return json_decode($this->CallAPI("POST", "invoices/".$invoice_id."/payments", [
            "datepay" => now(),
            "paymentid" => $mode_dolibarr_id,
            "closepaidinvoice" => null,
            "accountid" => 1,
            "amount" => $amount
        ]), true);
    }




}
