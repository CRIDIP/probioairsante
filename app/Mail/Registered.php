<?php

namespace App\Mail;

use App\Models\Shop;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Registered extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Shop
     */
    private $shop;

    /**
     * Create a new message instance.
     *
     * @param Shop $shop
     */
    public function __construct(Shop $shop)
    {
        //
        $this->shop = $shop;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->shop->email, $this->shop->name)
            ->subject('Bienvenue !')
            ->view('mail.registered', ["user" => auth()->user()]);
    }
}
