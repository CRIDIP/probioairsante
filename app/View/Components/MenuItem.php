<?php

namespace App\View\Components;

use Illuminate\View\Component;

class MenuItem extends Component
{
    private $href;
    private $active;
    /**
     * @var bool
     */
    private $icon;
    /**
     * @var bool
     */
    private $sub;
    /**
     * @var bool
     */
    private $subsub;

    /**
     * Create a new component instance.
     *
     * @param $href
     * @param $active
     * @param bool $icon
     * @param bool $sub
     * @param bool $subsub
     */
    public function __construct($href, $active, $icon = false, $sub = false, $subsub = false)
    {
        //
        $this->href = $href;
        $this->active = $active;
        $this->icon = $icon;
        $this->sub = $sub;
        $this->subsub = $subsub;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.menu-item');
    }
}
