<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Input extends Component
{
    private $name;
    private $type;
    private $label;
    /**
     * @var null
     */
    private $icon;
    /**
     * @var bool
     */
    private $require;
    /**
     * @var null
     */
    private $placeholder;

    /**
     * Create a new component instance.
     *
     * @param $name
     * @param $type
     * @param $label
     * @param null $icon
     * @param bool $require
     * @param null $placeholder
     */
    public function __construct($name, $type, $label, $icon = null, $require = false, $placeholder = null)
    {
        //
        $this->name = $name;
        $this->type = $type;
        $this->label = $label;
        $this->icon = $icon;
        $this->require = $require;
        $this->placeholder = $placeholder;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.input');
    }
}
