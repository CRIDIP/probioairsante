<?php

namespace App\DataTables;

use App\Models\State;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class StatesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($state) {
                return '
<a href="'.route('config.etats.edit', $state->id).'" class="btn btn-sm btn-icon btn-primary mr-3"><i class="fa fa-edit"></i> </a>
<a href="'.route('config.etats.alert', $state->id).'" class="btn btn-sm btn-icon btn-danger mr-3 '.($state->orders->count() ? 'disabled' : '').'"><i class="fa fa-trash"></i> </a>';
            })
            ->editColumn('color', function ($state) {
                return '<span class="label label-xl label-'.$state->color.' label-inline mr-2">'.$state->name.'</span>';
            })
            ->rawColumns(['action', 'color']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\State $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(State $model)
    {
        return $model->newQuery()->with('orders');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('states-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(5)
                    ->language('/json/french.json')
                    ->lengthMenu();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('name')->title('Nom'),
            Column::make('slug')->title('Slug'),
            Column::make('color')->title('Couleur')->addClass('text-center'),
            Column::make('indice')->title('indice')->addClass('text-center'),
            Column::computed('action')
                ->title('')
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'States_' . date('YmdHis');
    }
}
