<?php

namespace App\DataTables;

use App\Helpers\Generator;
use App\Models\Order;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class OrdersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at', function ($order) {
                return $order->created_at->calendar();
            })
            ->editColumn('updated_at', function ($order) {
                return $order->updated_at->calendar();
            })
            ->editColumn('total', function ($order) {
                return Generator::formatCurrency($order->total);
            })
            ->editColumn('payment', function ($order) {
                return $order->payment_text;
            })
            ->editColumn('state_id', function ($order) {
                return "<span class='label label-inline label-".$order->state->color."'>".$order->state->name."</span>";
            })
            ->orderColumn('state_id', '-state_id $1')
            ->addColumn('client', function ($order) {
                return '<a href="' . route('client.customers.show', $order->user->id) . '">' . $order->user->name . ' ' . $order->user->firstname . '</a>';
            })
            ->editColumn('invoice_id', function ($order) {
                return $order->invoice_id ? '<i class="fas fa-check text-success"></i>' : '';
            })
            ->addColumn('action', function ($order) {
                return '<a href="' . route('orders.show', $order->id) . '" class="btn btn-xs btn-info btn-block">Voir</a>';
            })
            ->rawColumns(['client', 'state_id', 'invoice_id', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param Order $model
     * @return \Illuminate\Database\Query\Builder
     */
    public function query(Order $model)
    {
        return $model->with('state', 'user')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('orders-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Blfrtip')
            ->orderBy(1)
            ->lengthMenu();
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id'),
            Column::make('reference')->title('Référence'),
            Column::computed('client')->title('Client'),
            Column::make('total')->title('Total'),
            Column::make('payment')->title('Paiement'),
            Column::make('state_id')->title('Etat'),
            Column::make('invoice_id')->title('Facture')->addClass('text-center'),
            Column::make('created_at')->title('Date'),
            Column::make('updated_at')->title('Changement'),
            Column::computed('action')->title('')->width(60)->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Orders_' . date('YmdHis');
    }
}
