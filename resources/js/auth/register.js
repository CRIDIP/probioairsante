function submitFormRegister() {
    let spinnerClass = 'spinner spinner-right spinner-white pr-15';

    let formFunction = () => {
        let form = KTUtil.getById('formRegister');
        let url = KTUtil.attr(form, 'action');
        let btn = KTUtil.getById('submitBtnRegisterForm');

        if (!form) {
            return;
        }

        FormValidation
            .formValidation(form, {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'Votre nom est requise'
                            }
                        }
                    },
                    firstname: {
                        validators: {
                            notEmpty: {
                                message: 'Votre prénom est requise'
                            }
                        }
                    },
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Votre adresse mail est requise'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Votre mot de passe est requis'
                            }
                        }
                    },
                    password_confirmation: {
                        validators: {
                            notEmpty: {
                                message: 'Votre mot de passe est requis'
                            },
                            different: {
                                compare: () => {
                                    return form.querySelector('#password').value
                                },
                                message: "Le mot de passe et sa confirmation sont différent."
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //	eleInvalidClass: '', // Repace with uncomment to hide bootstrap validation icons
                        //	eleValidClass: '',   // Repace with uncomment to hide bootstrap validation icons
                    })
                }
            })
            .on('core.form.valid', (e) => {
                e.preventDefault();
                KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter...');

                FormValidation.utils.fetch(url, {
                    method: 'POST',
                    dataType: 'json',
                    params: {
                        name: form.querySelector('#name').value,
                        firstname: form.querySelector('#firstname').value,
                        email: form.querySelector('#email').value,
                        password: form.querySelector('#password').value,
                        newsletter: form.querySelector('#newsletter').value,
                    },
                }).then((response) => {
                    KTUtil.btnRelease(btn);
                    if (response && typeof response === "object" && response.status && response.status === 'success') {
                        window.location.href='/'
                    }else{
                        swal.fire({
                            text: 'Oh Oh! Une erreur à eu lieu, veuillez réessayer plus tard !',
                            icon: 'error',
                            buttonsStyling: false,
                            confirmButtonText: "Ok, pas de problème!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(() => {
                            KTUtil.scrollTop()
                        })
                    }
                })
            })
            .on('core.form.invalid', function() {
                Swal.fire({
                    text: "Oh Oh !! Plusieurs erreurs ont eu lieu. Veuillez réessayer plus tard",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, pas de problème!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                }).then(function() {
                    KTUtil.scrollTop();
                });
            });
    }
}

jQuery(document).ready(function() {
    submitFormRegister();
});
