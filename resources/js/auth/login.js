function submitFormLogin() {
    let spinnerClass = 'spinner spinner-right spinner-white pr-15';

    let formFunction = () => {
        let form = KTUtil.getById('formLogin');
        let url = KTUtil.attr(form, 'action');
        let btn = KTUtil.getById('submitBtnLoginForm');

        if (!form) {
            return;
        }

        FormValidation
            .formValidation(form, {
                fields: {
                    email: {
                        validators: {
                            notEmpty: {
                                message: 'Votre adresse mail est requise'
                            }
                        }
                    },
                    password: {
                        validators: {
                            notEmpty: {
                                message: 'Votre mot de passe est requis'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    //defaultSubmit: new FormValidation.plugins.DefaultSubmit(), // Uncomment this line to enable normal button submit after form validation
                    bootstrap: new FormValidation.plugins.Bootstrap({
                        //	eleInvalidClass: '', // Repace with uncomment to hide bootstrap validation icons
                        //	eleValidClass: '',   // Repace with uncomment to hide bootstrap validation icons
                    })
                }
            })
            .on('core.form.valid', (e) => {
                e.preventDefault()
                KTUtil.btnWait(btn, spinnerClass, 'Veuillez patienter...')

                FormValidation.utils.fetch(url, {
                    method: 'POST',
                    dataType: 'json',
                    params: {
                        email: form.querySelector('#email').value,
                        password: form.querySelector('#password').value,
                    },
                }).then((response) => {
                    KTUtil.btnRelease(btn)
                    if (response && typeof response === "object" && response.status && response.status === 'success') {
                        window.location.href='/'
                    }else{
                        swal.fire({
                            text: 'Oh Oh! Une erreur à eu lieu, veuillez réessayer plus tard !',
                            icon: 'error',
                            buttonsStyling: false,
                            confirmButtonText: "Ok, pas de problème!",
                            customClass: {
                                confirmButton: "btn font-weight-bold btn-light-primary"
                            }
                        }).then(() => {
                            KTUtil.scrollTop()
                        })
                    }
                })
            })
            .on('core.form.invalid', function() {
                Swal.fire({
                    text: "Oh Oh !! Plusieurs erreurs ont eu lieu. Veuillez réessayer plus tard",
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: "Ok, pas de problème!",
                    customClass: {
                        confirmButton: "btn font-weight-bold btn-light-primary"
                    }
                }).then(function() {
                    KTUtil.scrollTop();
                });
            });
    }
}

jQuery(document).ready(function() {
    submitFormLogin();
});
