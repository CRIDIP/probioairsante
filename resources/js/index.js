import Splide from '@splidejs/splide'

new Splide('.splide', {
    type: 'loop',
    cover: true,
    height: '500px',
    autoplay: true,
}).mount();
