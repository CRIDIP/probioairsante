@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
    </div>
@endsection

@section('content')
    <!--begin::Container-->
    <div class=" container ">
        <div class="splide mb-10">
            <div class="splide__track">
                <ul class="splide__list">
                    <li class="splide__slide">
                        <img src="/images/slides/1.png" alt="">
                    </li>
                    <li class="splide__slide">
                        <img src="/images/slides/2.png" alt="">
                    </li>
                    <li class="splide__slide">
                        <img src="/images/slides/3.png" alt="">
                    </li>
                </ul>
            </div>
        </div>
        <div class="text-center font-weight-bold font-size-h1 mb-5 mt-5">Nos produits phare</div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-md-4">
                    <div class="card card-custom gutter-b card-stretch">
                        <div class="card-body d-flex flex-column rounded bg-light justify-content-between">
                            <div class="text-center rounded mb-7">
                                <img src="/images/thumbs/{{ $product->image }}" class="mw-100 w-200px">
                            </div>
                            <div>
                                <h4 class="font-size-h5"><a href="{{ route('produits.show', [$product->category_id, $product->id]) }}" class="text-dark-75 font-weight-bolder">{{ $product->name }}</a></h4>
                                @auth()
                                    @if($product->quantity)
                                        <div class="font-size-h6 text-muted font-weight-bolder">{{ \App\Helpers\Generator::formatCurrency($product->price) }}</div>
                                    @else
                                        <div class="font-size-h6 font-weight-bolder text-danger">Produit en rupture de stock</div>
                                    @endif
                                @else
                                    <div class="font-size-h6 font-weight-bolder text-danger">Vous devez être connecter pour voir le tarif</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <a href="{{ route('produits.index') }}">
                    <img src="{{ asset('images/content/bann_boutique.jpg') }}" alt="bann_boutique" class="img-fluid">
                </a>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <a href="https://francebilletreduc.shop"><img src="{{ asset('images/content/pbrs_resa_promo.jpg') }}" alt="1" class="img-fluid mb-5"></a>
                <img src="{{ asset('images/content/plaquette/1.jpg') }}" alt="1" class="img-fluid mb-5">
                <img src="{{ asset('images/content/plaquette/2.jpg') }}" alt="1" class="img-fluid mb-5">
                <img src="{{ asset('images/content/plaquette/3.jpg') }}" alt="1" class="img-fluid mb-5">
                <img src="{{ asset('images/content/plaquette/4.jpg') }}" alt="1" class="img-fluid mb-5">
                <img src="{{ asset('images/content/plaquette/5.jpg') }}" alt="1" class="img-fluid mb-5">
                <img src="{{ asset('images/content/plaquette/6.jpg') }}" alt="1" class="img-fluid mb-5">
            </div>
        </div>
    </div>
    <!--end::Container-->
@endsection

@section("script")
    <script src="{{ asset('js/index.js') }}"></script>
@endsection
