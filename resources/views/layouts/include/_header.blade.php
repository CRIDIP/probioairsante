<div id="kt_header" class="header flex-column  header-fixed" >
    <!--begin::Top-->
    <div class="header-top bg-warning max-h-55px">
        <!--begin::Container-->
        <div class=" container ">
            <!--begin::Left-->
            <div class="d-none d-lg-flex align-items-center mr-3">
                <!--begin::Logo-->
                <span class="text-white font-weight-bold font-size-h4"><i class="fa fa-phone text-white fa-lg"></i> Appelez-nous au 0899 492 648</span>
                <!--end::Logo-->
            </div>
            <!--end::Left-->

            <!--begin::Topbar-->
            <!--begin::Topbar-->
            <!--end::Topbar-->
            <div class="topbar bg-warning">
                @auth()
                        <!--begin::User-->
                        <div class="topbar-item">
                            <div class="btn btn-clean btn-icon w-auto d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                                <div class="d-flex text-right pr-3">
                                    <span class="text-dark opacity-50 font-weight-bold font-size-sm d-none d-md-inline mr-1">Bonjour,</span>
                                    <span class="text-dark font-weight-bolder font-size-sm d-none d-md-inline">{{ auth()->user()->name }} {{ auth()->user()->firstname }}</span>
                                </div>
                                <span class="symbol symbol-35">
									<span class="symbol-label font-size-h5 font-weight-bold bg-white-o-70">{{ \App\Helpers\Generator::firsLetter(auth()->user()->name) }}</span>
								</span>
                            </div>
                        </div>
                        <!--end::User-->
                @else
                        <a class="btn btn-clean font-weight-bold text-dark" href="{{ route('login') }}"><i class="la la-user-circle-o text-dark" style="font-size: 24px;"></i> Connexion</a>
                @endauth
                <!--begin::Tablet & Mobile Search-->
                <div class="dropdown d-flex d-lg-none">
                    <!--begin::Toggle-->
                    <div class="topbar-item" data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-icon btn-lg btn-dropdown mr-1">
	        				<span class="svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>	        			</div>
                    </div>
                    <!--end::Toggle-->

                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-anim-up dropdown-menu-lg">
                        <div class="quick-search quick-search-dropdown" id="kt_quick_search_dropdown">
                            <!--begin:Form-->
                            <form method="get" class="quick-search-form">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                <span class="input-group-text">
                    <span class="svg-icon svg-icon-lg"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>                </span>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Search..."/>
                                    <div class="input-group-append">
                <span class="input-group-text">
                    <i class="quick-search-close ki ki-close icon-sm text-muted"></i>
                </span>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->

                            <!--begin::Scroll-->
                            <div class="quick-search-wrapper scroll" data-scroll="true" data-height="325" data-mobile-height="200">
                            </div>
                            <!--end::Scroll-->
                        </div>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Tablet & Mobile Search-->

                <!--begin::My Cart-->
                <div class="dropdown">
                    <!--begin::Toggle-->
                    <div class="topbar-item"  data-toggle="dropdown" data-offset="10px,0px">
                        <div class="btn btn-icon btn-dropdown btn-lg mr-1 @if(Cart::getContent()->count() > 0) pulse pulse-primary @endif">
			                    <span class="svg-icon svg-icon-xl svg-icon-success"><!--begin::Svg Icon | path:assets/media/svg/icons/Shopping/Cart3.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M12,4.56204994 L7.76822128,9.6401844 C7.4146572,10.0644613 6.7840925,10.1217854 6.3598156,9.76822128 C5.9355387,9.4146572 5.87821464,8.7840925 6.23177872,8.3598156 L11.2317787,2.3598156 C11.6315738,1.88006147 12.3684262,1.88006147 12.7682213,2.3598156 L17.7682213,8.3598156 C18.1217854,8.7840925 18.0644613,9.4146572 17.6401844,9.76822128 C17.2159075,10.1217854 16.5853428,10.0644613 16.2317787,9.6401844 L12,4.56204994 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M3.5,9 L20.5,9 C21.0522847,9 21.5,9.44771525 21.5,10 C21.5,10.132026 21.4738562,10.2627452 21.4230769,10.3846154 L17.7692308,19.1538462 C17.3034221,20.271787 16.2111026,21 15,21 L9,21 C7.78889745,21 6.6965779,20.271787 6.23076923,19.1538462 L2.57692308,10.3846154 C2.36450587,9.87481408 2.60558331,9.28934029 3.11538462,9.07692308 C3.23725479,9.02614384 3.36797398,9 3.5,9 Z M12,17 C13.1045695,17 14,16.1045695 14,15 C14,13.8954305 13.1045695,13 12,13 C10.8954305,13 10,13.8954305 10,15 C10,16.1045695 10.8954305,17 12,17 Z" fill="#000000"/>
    </g>
</svg><!--end::Svg Icon--></span>
                            @if(Cart::getContent()->count() > 0)
                                <span class="pulse-ring"></span>
                            @endif
                        </div>
                    </div>
                    <!--end::Toggle-->

                    <!--begin::Dropdown-->
                    <div class="dropdown-menu p-0 m-0 dropdown-menu-right dropdown-menu-xl dropdown-menu-anim-up">
                        <form>
                            <!--begin::Header-->
                            <div class="d-flex align-items-center py-10 px-8 bgi-size-cover bgi-no-repeat rounded-top @if(Cart::isEmpty() == true) pulse pulse-primary @endif" style="background-image: url(assets/media/misc/bg-1.jpg)">
        <span class="btn btn-md btn-icon bg-white-o-15 mr-4">
            <i class="flaticon2-shopping-cart-1 text-success"></i>
        </span>
                                <h4 class="text-white m-0 flex-grow-1 mr-3">Mon Panier</h4>
                                <button type="button" class="btn btn-success btn-sm">{{ Cart::getContent()->count() }} @if(Cart::getContent()->count() > 1) articles @else article @endif</button>
                            </div>
                            <!--end::Header-->

                            <!--begin::Scroll-->
                            <div class="scroll scroll-push" data-scroll="true" data-height="250" data-mobile-height="200" >
                                @foreach(Cart::getContent()->toArray() as $cart)
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-center justify-content-between p-8">
                                        <div class="d-flex flex-column mr-2">
                                            <a href="#" class="font-weight-bold text-dark-75 font-size-lg text-hover-primary">
                                                {{ $cart['name'] }}
                                            </a>
                                            <span class="text-muted">
                                                {{ \Illuminate\Support\Str::limit($cart['associatedModel']['description'], 100, '...') }}
                                            </span>
                                            <div class="d-flex align-items-center mt-2">
                                                <span class="font-weight-bold mr-1 text-dark-75 font-size-lg">{{ \App\Helpers\Generator::formatCurrency($cart['price']) }}</span>
                                                <span class="text-muted mr-1">X</span>
                                                <span class="font-weight-bold mr-2 text-dark-75 font-size-lg">{{ $cart['quantity'] }}</span>
                                            </div>
                                        </div>
                                        <a href="#" class="symbol symbol-70 flex-shrink-0">
                                            <img src="/images/thumbs/{{ $cart['associatedModel']['image'] }}" title="" alt=""/>
                                        </a>
                                    </div>
                                    <!--end::Item-->
                                    @if(Cart::getContent()->count() > 1)
                                            <div class="separator separator-solid"></div>
                                    @endif
                                @endforeach

                                <!--begin::Separator-->

                                <!--end::Separator-->
                            </div>
                            <!--end::Scroll-->

                            <!--begin::Summary-->
                            <div class="p-8">
                                <div class="d-flex align-items-center justify-content-between mb-4">
                                    <span class="font-weight-bold text-muted font-size-sm mr-2">Sous Total</span>
                                    <span class="font-weight-bolder text-dark-50 text-right">{{ \App\Helpers\Generator::formatCurrency(Cart::getSubTotal()) }}</span>
                                </div>
                                <div class="d-flex align-items-center justify-content-between mb-7">
                                    <span class="font-weight-bold text-muted font-size-sm mr-2">Total (Hors Frais de port</span>
                                    <span class="font-weight-bolder text-primary text-right">{{ \App\Helpers\Generator::formatCurrency(Cart::getTotal()) }}</span>
                                </div>
                                <div class="text-right">
                                    <a href="{{ route('panier.index') }}" class="btn btn-primary text-weight-bold">Voir mon panier</a>
                                </div>
                            </div>
                            <!--end::Summary-->
                        </form>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::My Cart-->

                <a class="btn btn-clean font-weight-bold text-dark" href="https://pro.probioairsante.shop ">Compte Professionnel</a>
                <a class="btn btn-clean font-weight-bold text-dark" href="https://pro.probioairsante.shop">Compte Mairie / Collectivité</a>
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Container-->
    </div>

    <div class="header-top bg-light">
        <!--begin::Container-->
        <div class=" container ">
            <!--begin::Left-->
            <div class="d-none d-lg-flex align-items-center mr-3">
                <!--begin::Logo-->
                <a href="{{ route('home') }}" class="mr-10">
                    <img alt="Logo" src="/images/logo.png" class="max-h-55px"/>
                </a>
                <!--end::Logo-->
            </div>
            <!--end::Left-->

            <!--begin::Container-->
            <div class=" container ">
                <!--begin::Header Menu Wrapper-->
                <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                    <!--begin::Header Menu-->
                    <div id="kt_header_menu" class="header-menu header-menu-left header-menu-mobile  header-menu-layout-default " >
                        <!--begin::Header Nav-->
                        <ul class="menu-nav ">
                            <li class="menu-item "  aria-haspopup="true">
                                <a  href="{{ route('home') }}" class="menu-link ">
                                    <span class="menu-text">Acceuil</span>
                                </a>
                            </li>
                            <li class="menu-item "  aria-haspopup="true">
                                <a  href="{{ route('produits.index') }}" class="menu-link ">
                                    <span class="menu-text">Boutique</span>
                                </a>
                            </li>
                            @foreach($pages::pageAsMenu() as $page)
                                <li class="menu-item "  aria-haspopup="true">
                                    <a  href="{{ route('page', $page->slug) }}" class="menu-link ">
                                        <span class="menu-text">{{ $page->title }}</span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <!--end::Header Nav-->
                    </div>
                    <!--end::Header Menu-->
                </div>
                <!--end::Header Menu Wrapper-->

                <!--begin::Desktop Search-->
                <div class="d-none d-lg-flex align-items-center">
                    <div class="quick-search quick-search-inline ml-4 w-250px" id="kt_quick_search_inline">
                        <!--begin::Form-->
                        <form method="get" class="quick-search-form">
                            <div class="input-group rounded">
                                <div class="input-group-prepend">
								<span class="input-group-text">
									<span class="svg-icon svg-icon-lg"><!--begin::Svg Icon | path:assets/media/svg/icons/General/Search.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>
        <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"/>
    </g>
</svg><!--end::Svg Icon--></span>								</span>
                                </div>

                                <input type="text" class="form-control h-40px" placeholder="Search..."/>

                                <div class="input-group-append">
								<span class="input-group-text">
									<i class="quick-search-close ki ki-close icon-sm"></i>
								</span>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->

                        <!--begin::Search Toggle-->
                        <div id="kt_quick_search_toggle" data-toggle="dropdown" data-offset="0px,0px"></div>
                        <!--end::Search Toggle-->

                        <!--begin::Dropdown-->
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-lg dropdown-menu-anim-up">
                            <div class="quick-search-wrapper scroll" data-scroll="true" data-height="350" data-mobile-height="200">
                            </div>
                        </div>
                        <!--end::Dropdown-->
                    </div>
                </div>
                <!--end::Desktop Search-->
            </div>
            <!--end::Container-->

        </div>
        <!--end::Container-->
    </div>
    <!--end::Top-->
</div>
