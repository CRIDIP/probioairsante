<!--begin::Footer-->
<div class="footer bg-dark py-4 d-flex flex-lg-column" id="kt-footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="font-weight-bold font-size-h3-lg text-white mb-3">Services</div>
                <ul class="list-unstyled text-white">
                    @foreach($pages::pageAsNotMenu() as $page)
                    <li>
                        <a href="{{ route('page', $page->slug) }}" class="text-white">{{ $page->title }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-3">
                <div class="font-weight-bold font-size-h3-lg text-white mb-3">Notre Gamme PRO</div>
                <ul class="list-unstyled">
                    <li><a href="https://pro.probioairsante.shop" class="text-white">Compte Professionnel</a></li>
                    <li><a href="https://collectivite.probioairsante.shop" class="text-white">Compte Mairie / Collectivité</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="font-weight-bold font-size-h3-lg text-white mb-3">Votre compte</div>
                <ul class="list-unstyled">
                    <li><a href="{{ route('account.identite.edit') }}" class="text-white">Informations Personnelles</a></li>
                    <li><a href="{{ route('account.command.index') }}" class="text-white">Commandes</a></li>
                    <li><a href="{{ route('account.adresses.index') }}" class="text-white">Adresses</a></li>
                </ul>
            </div>
            <div class="col-md-3">
                <div class="font-weight-bold font-size-h3-lg text-white mb-3">Informations</div>
                <p class="text-white">
                    {{ $shop->name }}<br>
                    {{ $shop->address }}<br>
                    <i class="fa fa-phone"></i> {{ $shop->phone }}<br>
                    <i class="fa fa-envelope"></i> {{ $shop->email }}<br>
                </p>
            </div>
        </div>
    </div>
</div>
<div class="footer bg-white py-4 d-flex flex-lg-column " id="kt_footer">
    <!--begin::Container-->
    <div class=" container  d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted font-weight-bold mr-2">2020&copy;</span>
            <a href="http://keenthemes.com/metronic" target="_blank" class="text-dark-75 text-hover-primary">Probioairsante.shop</a>
        </div>
        <!--end::Copyright-->

        <!--begin::Nav-->
        <!--<div class="nav nav-dark order-1 order-md-2">
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pr-3 pl-0">About</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link px-3">Team</a>
            <a href="http://keenthemes.com/metronic" target="_blank" class="nav-link pl-3 pr-0">Contact</a>
        </div>-->
        <!--end::Nav-->
    </div>
    <!--end::Container-->
</div>
<!--end::Footer-->
