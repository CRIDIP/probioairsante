<div class="card card-custom gutter-b" id="payment-pending">
    <div class="card-body">
        <div class="font-size-h4">Vous avez choisi de payer par carte bancaire. Veuillez compléter le présent formulaire pour procéder à ce règlement</div>
        <div class="alert alert-custom alert-light-danger fade show mb-5" role="alert">
            <div class="alert-icon">
                <i class="flaticon-info"></i>
            </div>
            <div class="alert-text">
                <p>
                    Nous ne conservons aucune de ces informations sur notre site, elles sont directement transmises à notre prestataire de paiement <a href="https://stripe.com/fr">Stripe</a>.<br>
                    La transmission de ces informations est entièrement sécurisée.
                </p>
            </div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">
						<i class="ki ki-close"></i>
					</span>
                </button>
            </div>
        </div>
        <form id="payment-form">
            <div class="row">
                <div class="col-md-9">
                    <div class="card blue-grey darken-1">
                        <div class="card-content white-text">
                            <div id="card-element"></div>
                        </div>
                    </div>
                    <div id="card-errors" class="helper-text text-danger"></div>
                </div>
                <div class="col-md-3">
                    <button type="submit" id="btnSubmitStripeForm" name="action" class="btn btn-block btn-primary btn-hover-bg-blue"><i class="fa fa-check-circle"></i> Payer</button>
                </div>
            </div>
        </form>
    </div>
</div>
