<div class="font-weight-bold font-size-h5-lg">Détail de la commande</div>
<div class="table-responsive">
    <table class="table">
        <thead>
        <tr>
            <th>Produit</th>
            <th class="text-center">Qte</th>
            <th class="text-center">Prix Unitaire</th>
            <th class="text-center">Montant total</th>
        </tr>
        </thead>
        <tbody>
        @foreach($content as $item)
            <tr>
                <td class="d-flex align-items-center font-weight-bolder">
                    {{ $item->name }}
                </td>
                <td class="text-center align-middle">
                    <span class="mr-2 font-weight-bolder">{{ $item->quantity }}</span>
                </td>
                <td class="text-center align-middle">
                    <span class="mr-2 font-weight-bolder">{{ \App\Helpers\Generator::formatCurrency($item->total_price_gross / $item->quantity) }}</span>
                </td>
                <td class="text-center align-middle">
                    <span class="mr-2 font-weight-bolder">{{ \App\Helpers\Generator::formatCurrency($item->total_price_gross ?? ($tax > 0 ? $item->price : $item->price / 1.2) * $item->quantity) }}</span>
                </td>
            </tr>
        @endforeach
        </tbody>
        <tfoot>
        <tr>
            <td colspan="2"></td>
            <td class="font-weight-bold font-size-h4 text-right">Sous-Total</td>
            <td class="font-weight-bolder font-size-h4 text-right">{{ \App\Helpers\Generator::formatCurrency($total) }}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="font-weight-bold font-size-h4 text-right">Livraison</td>
            <td class="font-weight-bolder font-size-h4 text-right">{{ \App\Helpers\Generator::formatCurrency($shipping) }}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="font-weight-bold font-size-h4 text-right">TVA ({{ $tax * 100 }}%)</td>
            <td class="font-weight-bolder font-size-h4 text-right">{{ \App\Helpers\Generator::formatCurrency($total / (1 + $tax) * $tax) }}</td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td class="font-weight-bold font-size-h4-lg text-right">Total TTC</td>
            <td class="font-weight-bolder font-size-h4-lg text-right">{{ \App\Helpers\Generator::formatCurrency($total + $shipping) }}</td>
        </tr>
        </tfoot>
    </table>
</div>
