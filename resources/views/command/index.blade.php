@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Ma Commande</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <form action="{{ route('commandes.store') }}" method="POST" id="formCheckout" class="form">
            @csrf
            @include("components.alert")
            <div class="accordion accordion-toggle-arrow" id="accordionExample1">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title" data-toggle="collapse" data-target="#collapseOne1">
                            Ma Commande
                        </div>
                    </div>
                    <div id="collapseOne1" class="collapse show" data-parent="#accordionExample1">
                        <div class="card-body">
                            <div class="font-size-h5 font-weight-bold">Adresse de facturation et de livraison</div>
                            @include('command.partials.addresses', ['name' => 'facturation'])
                            <div class="row">
                                <div class="col-12">
                                    <a href="" class="btn btn-block btn-info"><i class="la la-map-marked"></i> Gérer mes adresses</a>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col s12">
                                    <label>
                                        <input id="different" name="different" type="checkbox" @if($addresses->count() === 1)  disabled="disabled" @endif>
                                        <span>
                @if($addresses->count() === 1)
                                                Vous n'avez qu'une adresse enregistrée, si vous voulez une adresse différente pour la livraison <a href="{{ route('account.adresses.create') }}">vous pouvez en créer une autre</a>.
                                            @else
                                                Mon adresse de livraison est différente de mon adresse de facturation
                                            @endif
              </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseTwo1">
                            Mode de livraison
                        </div>
                    </div>
                    <div id="collapseTwo1" class="collapse" data-parent="#accordionExample1">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <label class="option">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="expedition" value="colissimo" checked="checked">
											<span></span>
										</span>
									</span>
                                        <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                                <img src="/images/expedition/colissimo.png" alt="" width="30">
												Colissimo
											</span>
										</span>
										<span class="option-body">
											Délai estimé: 5 jours
										</span>
									</span>
                                    </label>
                                </div>
                                <div class="col-md-12">
                                    <label class="option">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="expedition" value="ups">
											<span></span>
										</span>
									</span>
                                        <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                            <img src="/images/expedition/ups.png" alt="" width="30">
												UPS
											</span>
										</span>
										<span class="option-body">
											Délai estimé: 2 jours
										</span>
									</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseThree1">
                            Paiement
                        </div>
                    </div>
                    <div id="collapseThree1" class="collapse" data-parent="#accordionExample1">
                        <div class="card-body">
                            @if($shop->card)
                                <label class="option option-plain">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="payment" value="carte" checked="checked">
											<span></span>
										</span>
									</span>
                                    <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                                <img src="/images/payment/credit-card.png" alt="" width="30">
                                                Carte Bancaire
                                            </span>
										</span>
										<span class="option-body">Vous devrez renseigner un formulaire de paiement sur la page de confirmation de cette commande.</span>
									</span>
                                </label>
                            @endif
                            @if($shop->mandat)
                                    <label class="option option-plain">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="payment" value="mandat" checked="checked">
											<span></span>
										</span>
									</span>
                                        <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                                <img src="/images/payment/mandat.png" alt="" width="30">
                                                Mandat Administratif
                                            </span>
										</span>
										<span class="option-body">Envoyez un bon de commande avec la mention "Bon pour accord". Votre commande sera expédiée dès réception de ce bon de commande. N'oubliez pas de préciser la référence de la commande dans votre bon.</span>
									</span>
                                    </label>
                            @endif
                            @if($shop->transfer)
                                    <label class="option option-plain">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="payment" value="virement" checked="checked">
											<span></span>
										</span>
									</span>
                                        <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                                <img src="/images/payment/bank-transfer.png" alt="" width="30">
                                                Virement Bancaire
                                            </span>
										</span>
										<span class="option-body">Il vous faudra transférer le montant de la commande sur notre compte bancaire. Vous recevrez votre confirmation de commande comprenant nos coordonnées bancaires et le numéro de commande. Les biens seront mis de côté 30 jours pour vous et nous traiterons votre commande dès la réception du paiement. </span>
									</span>
                                    </label>
                            @endif
                            @if($shop->check)
                                    <label class="option option-plain">
									<span class="option-control">
										<span class="radio">
											<input type="radio" name="payment" value="cheque" checked="checked">
											<span></span>
										</span>
									</span>
                                        <span class="option-label">
										<span class="option-head">
											<span class="option-title">
                                                <img src="/images/payment/check.png" alt="" width="30">
                                                Chèque Bancaire
                                            </span>
										</span>
										<span class="option-body">Il vous faudra nous envoyer un chèque du montant de la commande. Vous recevrez votre confirmation de commande comprenant nos coordonnées bancaires et le numéro de commande. Les biens seront mis de côté 30 jours pour vous et nous traiterons votre commande dès la réception du paiement. </span>
									</span>
                                    </label>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFour1">
                            Détail de la commande
                        </div>
                    </div>
                    <div id="collapseFour1" class="collapse" data-parent="#accordionExample1">
                        <div class="card-body">
                            @include("command.partials.detail")
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title collapsed" data-toggle="collapse" data-target="#collapseFive1">
                            Confirmation de la commande
                        </div>
                    </div>
                    <div id="collapseFive1" class="collapse" data-parent="#accordionExample1">
                        <div class="card-body">
                            <label class="checkbox">
                                <input type="checkbox" name="ok" id="ok">
                                <span></span>
                                J'ai lu <a href="#" target="_blank">les conditions générales de vente et les conditions d'annulation</a> et j'y adhère sans réserve.
                            </label>
                            <br>
                            <button type="submit" id="btnSubmitCheckout" class="btn btn-success" disabled><i class="fa fa-check"></i> Commande avec obligation de paiement</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@section("script")
    <script type="text/javascript">


        (function ($) {
            document.addEventListener('DOMContentLoaded', () => {
                document.querySelector('#ok').addEventListener('change', () => {
                    document.querySelector('#btnSubmitCheckout').removeAttribute('disabled');
                })
            });

            $("#formCheckout").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formCheckout")
                let url = form.attr('action')
                let btn = KTUtil.getById('btnSubmitCheckout')
                let data = form.serializeArray()

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter')

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        201: (data) => {
                            KTUtil.btnRelease(btn)
                            toastr.success("Commande Valider")
                            setTimeout(() => {
                                window.location.href='/commandes/confirmation/'+data
                            })
                        },
                        202: (data) => {
                            KTUtil.btnRelease(btn)
                            toastr.warning(data, "Stock insuffisant")
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn)
                            toastr.error("Erreur de script");
                        }
                    }
                })
            })
        })(jQuery)
    </script>
@endsection
