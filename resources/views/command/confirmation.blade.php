@extends('layouts.app')

@section("style")
    <style>
        .StripeElement {
            box-sizing: border-box;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }
        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }
        .StripeElement--invalid {
            border-color: #fa755a;
        }
        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Confirmation de votre commande N° {{ $order->reference }}</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="font-size-h5 font-weight-bold">Votre commande est confirmé</div>
                <p>Un email vous à été envoyé à l'adresse <i>{{ auth()->user()->email }}</i> avec toutes ces informations.</p>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                @include('command.partials.detail', [
                    'tax' => $order->tax,
                    'shipping' => $order->shipping,
                    'total' => $order->total,
                    'content' => $order->products,
                  ])

                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <td>Référence de la commande</td>
                            <td>{{ $order->reference }}</td>
                        </tr>
                        <tr>
                            <td>Moyen de paiement</td>
                            <td>{{ $order->payment_text }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="font-weight-bold font-size-h5">Adresse de facturation @if($order->adresses->count() === 1) et de livraison @endif</div>
                @include('account.addresses.partials.address', [
                    'address' => $order->adresses->first(),
                  ])
                @if($order->adresses->count() > 1)
                    <h5>Adresse de livraison</h5>
                    @include('account.addresses.partials.address', [
                      'address' => $order->adresses->last(),
                    ])
                @endif
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-body">
                <div class="font-size-h5 font-weight-bold">Votre commande a bien été enregistrée.</div>
                @if($order->state->slug === 'cheque')
                    <p>Veuillez nous envoyer un chèque avec les informations suivantes:</p>
                    <ul>
                        <li>Montant du règlement: <strong>{{ \App\Helpers\Generator::formatCurrency($order->total * (1 + $order->tax) + $order->shipping) }}</strong></li>
                        <li>Payable à l'ordre de: <strong>{{ $shop->name }}</strong></li>
                        <li>A envoyer à <strong>{{ $shop->address }}</strong></li>
                        <li>N'oubliez pas d'indiquer la référence de votre commande <strong>{{ $order->reference }}</strong></li>
                    </ul>
                    @if($order->pick)
                    <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Vous pourrez venir chercher votre commande dès réception du paiement.</div>
                    @else
                    <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Votre commande vous sera envoyée dès réception du paiement</div>
                    @endif
                @elseif($order->state->slug === 'mandat')
                    <p>Vous avez choisi de payer par mandat administratif. Ce type de paiement est réservé aux administrations. </p>
                    <p>Vous devez envoyer votre mandat administratif à :</p>
                    <p><strong>{{ $shop->name }}</strong></p>
                    <p><strong>{{ $shop->address }}</strong></p>
                    <p>Vous pouvez aussi nous le transmettre par e-mail à cette adresse : <strong>{{ $shop->email }}</strong></p>
                    <p>N'oubliez pas d'indiquer votre référence de commande <strong>{{ $order->reference }}</strong>.</p>
                    @if($order->pick)
                        <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Vous pourrez venir chercher votre commande dès réception du paiement.</div>
                    @else
                        <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Votre commande vous sera envoyée dès réception du paiement</div>
                    @endif
                @elseif($order->state->slug === 'virement')
                    <p>Veuillez effectuer un virement sur notre compte :</p>
                    <ul>
                        <li>- montant du virement : <strong>{{ number_format($order->total * (1 + $order->tax) + $order->shipping, 2, ',', ' ') }} €</strong></li>
                        <li>- titulaire : <strong>{{ $shop->holder }}</strong></li>
                        <li>- BIC : <strong>{{ $shop->bic }}</strong></li>
                        <li>- IBAN : <strong>{{ $shop->iban }}</strong></li>
                        <li>- banque : <strong>{{ $shop->bank }}</strong></li>
                        <li>- adresse banque : <strong>{{ $shop->bank_address }}</strong></li>
                        <li>- n'oubliez pas d'indiquer votre référence de commande <strong>{{ $order->reference }}</strong></li>
                    </ul>
                    @if($order->pick)
                        <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Vous pourrez venir chercher votre commande dès réception du paiement.</div>
                    @else
                        <div class="alert alert-info" role="alert"><i class="la la-map-pin"></i> Votre commande vous sera envoyée dès réception du paiement</div>
                    @endif
                @elseif($order->state->slug === 'carte' || $order->state->slug === 'erreur')
                    @include('command.partials.stripe')
                @endif
                <div id="payment_ok">
                    <div class="card card-custom">
                        <div class="card-header border-0 bg-success">
                            <div class="card-title">
								<span class="card-icon">
									<i class="flaticon2-check-mark text-white"></i>
								</span>
                                <h3 class="card-label text-white">Votre paiement à été valider</h3>
                            </div>
                        </div>
                        <div class="separator separator-solid separator-white opacity-20"></div>
                        <div class="card-body text-white">
                            @if($order->pick)
                                <p>Vous pouvez venir chercher votre commande</p>
                            @else
                                <p>Votre commande va vous être envoyée</p>
                            @endif
                            <p>Pour toute question ou information complémentaire merci de contacter notre support client.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    @include('command.partials.stripejs')
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
