@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">
                        {{ $product->name }}	                	            </h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
<div class="container">
    @if(session()->has('cart'))
        <div class="modal fade" id="addingCart" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="staticBackdrop" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Produit Ajouter au panier</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <span class="font-weight-bold"><i class="la la-check-circle text-secondary"></i> Le produit à été ajouté au panier avec succès</span><br>
                        <p>Il y a {{ Cart::getContent()->count() }} @if(Cart::getContent()->count() > 1) articles @else article @endif dans votre panier pour un total de <strong>{{ \App\Helpers\Generator::formatCurrency(\Cart::getTotal()) }}</strong> TTC (hors frais de port)</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Continuer mes achats</button>
                        <a href="{{ route('panier.index') }}" class="btn btn-primary font-weight-bold">Commander <i class="flaticon2-shopping-cart-1"></i></a>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-md-4">
            <div class="card card-custom">
                <div class="card-body">
                    <img src="/images/thumbs/{{ $product->image }}" alt="{{ $product->name }}" class="img-fluid" />
                </div>
            </div>
        </div>
        <div class="col-md-1">&nbsp;</div>
        <div class="col-md-7">
            <div class="card card-custom gutter-b">
                <div class="card-header border-0 pt-5">
                    <h3 class="card-title"><span class="card-label font-weight-bolder text-dark mb-1">{{ $product->name }}</span></h3>
                    <div class="card-toolbar">
                        @auth()
                        <span class="font-weight-bold font-size-h5 text-primary">{{ \App\Helpers\Generator::formatCurrency($product->price) }}</span>
                        @else
                            <span class="font-weight-bold font-size-h5 text-danger">Vous devez être connecter pour voir le tarif</span>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <p>{!! $product->description !!}</p>
                    <form class="form" action="{{ route('panier.store') }}" method="POST">
                        @csrf
                        <input type="hidden" id="id" name="id" value="{{ $product->id }}">
                        <div class="form-group row">
                            <label for="quantity" class="col-form-label col-md-3">Quantité</label>
                            <div class="col-md-3">
                                <input id="quantity" name="quantity" type="number" class="form-control" value="1" min="1">
                            </div>
                            <div class="col-md-6">
                                <button @if(!$product->quantity) disabled  @endif class="btn btn-primary btn-shadow font-weight-bold mr-2"><i class="la la-shopping-cart"></i> Ajouter au panier</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    @if(session()->has('cart'))
    <script type="text/javascript">
        (function ($) {
            $("#addingCart").modal('show')
        })(jQuery)
    </script>
    @endif
@endsection
