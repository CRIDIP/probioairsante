@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Boutique / {{ $category->name }}</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-custom">
                <div class="card-body">
                    <ul class="navi navi-bolder">
                        <li class="navi-item">
                            <a href="{{ route('produits.index') }}" class="navi-link">
                                <span class="navi-icon"><i class="fa fa-dot-circle"></i> </span>
                                <span class="navi-text">Boutique</span>
                            </a>
                        </li>
                        @foreach($categories as $category)
                            <li class="navi-item">
                                <a href="{{ route('produits.category', $category->id) }}" class="navi-link">
                                    <span class="navi-icon"><i class="fa fa-dot-circle"></i> </span>
                                    <span class="navi-text">{{ $category->name }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card card-custom">
                <div class="card-body">
                    <div class="row">
                        @foreach($products as $product)
                            <div class="col-md-4">
                                <div class="card card-custom gutter-b card-stretch">
                                    <div class="card-body d-flex flex-column rounded bg-light justify-content-between">
                                        <div class="text-center rounded mb-7">
                                            <img src="/images/thumbs/{{ $product->image }}" class="img-fluid">
                                        </div>
                                        <div>
                                            <h4 class="font-size-h5"><a href="{{ route('produits.show', [$product->category_id, $product->id]) }}" class="text-dark-75 font-weight-bolder">{{ $product->name }}</a></h4>
                                            @auth()
                                                @if($product->quantity)
                                                    <div class="font-size-h6 text-muted font-weight-bolder">{{ \App\Helpers\Generator::formatCurrency($product->price) }}</div>
                                                @else
                                                    <div class="font-size-h6 font-weight-bolder text-danger">Produit en rupture de stock</div>
                                                @endif
                                            @else
                                                <div class="font-size-h6 font-weight-bolder text-danger">Vous devez être connecter pour voir le tarif</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("script")
    @if(session()->has('cart'))
    <script type="text/javascript">
        (function ($) {
            $("#addingCart").modal('show')
        })(jQuery)
    </script>
    @endif
@endsection
