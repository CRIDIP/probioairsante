<!--begin::Aside-->
<div class="aside aside-left  aside-fixed  d-flex flex-column flex-row-auto" id="kt_aside">
    <!--begin::Brand-->
    <div class="brand flex-column-auto " id="kt_brand">
        <!--begin::Logo-->
        <a href="{{ route('admin') }}" class="brand-logo">
            <img alt="Logo" src="/images/logo.png" width="200"/>
        </a>
        <!--end::Logo-->

        <!--begin::Toggle-->
        <button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
				<span class="svg-icon svg-icon svg-icon-xl"><!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg--><svg
                        xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                        height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <polygon points="0 0 24 0 24 24 0 24"/>
        <path
            d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
            fill="#000000" fill-rule="nonzero"
            transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) "/>
        <path
            d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
            fill="#000000" fill-rule="nonzero" opacity="0.3"
            transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) "/>
    </g>
</svg><!--end::Svg Icon--></span></button>
        <!--end::Toolbar-->
    </div>
    <!--end::Brand-->

    <!--begin::Aside Menu-->
    <div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">

        <!--begin::Menu Container-->
        <div
            id="kt_aside_menu"
            class="aside-menu my-4 "
            data-menu-vertical="1"
            data-menu-scroll="1" data-menu-dropdown-timeout="500">
            <!--begin::Menu Nav-->
            <ul class="menu-nav ">
                <li class="menu-item {{ currentRouteActive('admin') }}" aria-haspopup="true">
                    <a href="{{ route('admin') }}" class="menu-link ">
                        <i class="menu-icon flaticon-home"></i>
                        <span class="menu-text">Tableau de bord</span>
                    </a>
                </li>
                <li class="menu-item  menu-item-submenu {{ menuOpen(
    'config.boutique', 'config.boutique.update', 'config.pays.index', 'config.pays.create', 'config.pays.edit',
    'config.etats.index','config.etats.create','config.etats.edit','config.etats.alert',
    'config.pages.index','config.pages.create','config.pages.edit','config.pages.alert',
    'config.maintenance.edit'
    ) }}" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <i class="menu-icon flaticon-cogwheel"></i>
                        <span class="menu-text">Administration</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu ">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item {{ currentRouteActive('config.boutique', 'config.boutique.update') }}" aria-haspopup="true">
                                <a href="{{ route('config.boutique') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Boutique</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('config.pays.index', 'config.pays.create', 'config.pays.edit') }}" aria-haspopup="true">
                                <a href="{{ route('config.pays.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Pays</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('config.etats.index','config.etats.create','config.etats.edit','config.etats.alert') }}" aria-haspopup="true">
                                <a href="{{ route('config.etats.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Etat des commandes</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('config.pages.index','config.pages.create','config.pages.edit','config.pages.alert') }}" aria-haspopup="true">
                                <a href="{{ route('config.pages.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Pages</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('config.maintenance.edit') }}" aria-haspopup="true">
                                <a href="{{ route('config.maintenance.edit') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Maintenance</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item {{ currentRouteActive('orders.index', 'orders.show') }}" aria-haspopup="true">
                    <a href="{{ route('orders.index') }}" class="menu-link ">
                        <i class="menu-icon flaticon2-shopping-cart"></i>
                        <span class="menu-text">Commandes</span>
                    </a>
                </li>
                <li class="menu-item  menu-item-submenu {{ menuOpen('client.customers.index', 'client.customers.show') }}" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <i class="menu-icon flaticon-users"></i>
                        <span class="menu-text">Clients</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu ">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item {{ currentRouteActive('client.customers.index', 'client.customers.show') }}" aria-haspopup="true">
                                <a href="{{ route('client.customers.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Client</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item  menu-item-submenu {{ menuOpen(
    'catalog.category.index', 'catalog.category.create', 'catalog.category.edit', 'catalog.category.alert',
    'catalog.product.index', 'catalog.product.create', 'catalog.product.edit', 'catalog.product.alert'
) }}" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <i class="menu-icon flaticon2-cube"></i>
                        <span class="menu-text">Catalogue</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu ">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item {{ currentRouteActive('catalog.category.index', 'catalog.category.create', 'catalog.category.edit', 'catalog.category.alert') }}" aria-haspopup="true">
                                <a href="{{ route('catalog.category.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Catégories</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('catalog.product.index', 'catalog.product.create', 'catalog.product.edit', 'catalog.product.alert') }}" aria-haspopup="true">
                                <a href="{{ route('catalog.product.index') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Produits</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item  menu-item-submenu {{ menuOpen('exp.range.edit', 'exp.colissimo.edit') }}" aria-haspopup="true" data-menu-toggle="hover">
                    <a href="javascript:;" class="menu-link menu-toggle">
                        <i class="menu-icon flaticon-truck"></i>
                        <span class="menu-text">Expédition</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="menu-submenu ">
                        <i class="menu-arrow"></i>
                        <ul class="menu-subnav">
                            <li class="menu-item {{ currentRouteActive('exp.range.edit') }}" aria-haspopup="true">
                                <a href="{{ route('exp.range.edit') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Plages</span>
                                </a>
                            </li>
                            <li class="menu-item {{ currentRouteActive('exp.colissimo.edit') }}" aria-haspopup="true">
                                <a href="{{ route('exp.colissimo.edit') }}" class="menu-link ">
                                    <i class="menu-bullet menu-bullet-line">
                                        <span></span>
                                    </i>
                                    <span class="menu-text">Tarification</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
            <!--end::Menu Nav-->
        </div>
        <!--end::Menu Container-->
    </div>
    <!--end::Aside Menu-->
</div>
<!--end::Aside-->
