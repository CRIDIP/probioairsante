@if($active == true)
    <div class="subheader py-2 py-lg-6  subheader-solid " id="kt_subheader">
        <div class=" container-fluid  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">{{ $titlePage }}</h5>
                    <!--end::Page Title-->

                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        @foreach($breads as $bread)
                            @if($bread['last'] == false)
                                <li class="breadcrumb-item">
                                    <a href="{{ $bread['url'] }}" class="text-muted">{{ $bread['name'] }}</a>
                                </li>
                            @else
                                <li class="breadcrumb-item">
                                    <span class="font-weight-bold">{{ $bread['name'] }}</span>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <!--begin::Actions-->
                @if($returnControl == true)
                    <a class="btn btn-light-primary font-weight-bolder btn-sm" onclick="history.back()"><i class="la la-arrow-circle-left"></i> Retour</a>
                @endif
                @if($btns)
                    @foreach($btns as $btn)
                        <a href="{{ $btn['url'] }}" class="{{ $btn['class'] }}"><i class="{{ $btn['icon'] }}"></i> {{ $btn['name'] }}</a>
                    @endforeach
                @endif
                <!--end::Actions-->
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endif
