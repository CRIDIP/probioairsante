@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Commande ".$order->reference." de ".$order->user->name." ".$order->user->firstname,
    "breads" => [
        [
            "name" => "Accueil",
            "url" => "/",
            "last" => false
        ],
        [
            "name" => "Commandes",
            "url" => route('orders.index'),
            "last" => false
        ],
        [
            "name" => "Commande ".$order->reference." de ".$order->user->name." ".$order->user->firstname,
            "url" => "/",
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => [

    ]
])
@endsection

@section("content")
    <div class="card card-custom gutter-b">
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="card card-custom bgi-no-repeat card-stretch" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                        <!--begin::Body-->
                        <div class="card-body">
												<span class="svg-icon svg-icon-2x svg-icon-info">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <rect fill="#000000" x="2" y="4" width="19" height="4" rx="1"/>
        <path d="M3,10 L6,10 C6.55228475,10 7,10.4477153 7,11 L7,19 C7,19.5522847 6.55228475,20 6,20 L3,20 C2.44771525,20 2,19.5522847 2,19 L2,11 C2,10.4477153 2.44771525,10 3,10 Z M10,10 L13,10 C13.5522847,10 14,10.4477153 14,11 L14,19 C14,19.5522847 13.5522847,20 13,20 L10,20 C9.44771525,20 9,19.5522847 9,19 L9,11 C9,10.4477153 9.44771525,10 10,10 Z M17,10 L20,10 C20.5522847,10 21,10.4477153 21,11 L21,19 C21,19.5522847 20.5522847,20 20,20 L17,20 C16.4477153,20 16,19.5522847 16,19 L16,11 C16,10.4477153 16.4477153,10 17,10 Z" fill="#000000" opacity="0.3"/>
    </g>
</svg>
                                                    <!--end::Svg Icon-->
												</span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $order->created_at->calendar() }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Date de création</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-custom bgi-no-repeat card-stretch" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                        <!--begin::Body-->
                        <div class="card-body">
												<span class="svg-icon svg-icon-2x svg-icon-success">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z" fill="#000000" opacity="0.3" transform="translate(11.500000, 12.000000) rotate(-345.000000) translate(-11.500000, -12.000000) "/>
        <path d="M2,6 L21,6 C21.5522847,6 22,6.44771525 22,7 L22,17 C22,17.5522847 21.5522847,18 21,18 L2,18 C1.44771525,18 1,17.5522847 1,17 L1,7 C1,6.44771525 1.44771525,6 2,6 Z M11.5,16 C13.709139,16 15.5,14.209139 15.5,12 C15.5,9.790861 13.709139,8 11.5,8 C9.290861,8 7.5,9.790861 7.5,12 C7.5,14.209139 9.290861,16 11.5,16 Z M11.5,14 C12.6045695,14 13.5,13.1045695 13.5,12 C13.5,10.8954305 12.6045695,10 11.5,10 C10.3954305,10 9.5,10.8954305 9.5,12 C9.5,13.1045695 10.3954305,14 11.5,14 Z" fill="#000000"/>
    </g>
</svg>
                                                    <!--end::Svg Icon-->
												</span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ \App\Helpers\Generator::formatCurrency($order->total) }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Total de la commande</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-custom bgi-no-repeat card-stretch" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                        <!--begin::Body-->
                        <div class="card-body">
												<span class="svg-icon svg-icon-2x svg-icon-primary">
													<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist-->
													<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <rect x="0" y="0" width="24" height="24"/>
        <path d="M4,9.67471899 L10.880262,13.6470401 C10.9543486,13.689814 11.0320333,13.7207107 11.1111111,13.740321 L11.1111111,21.4444444 L4.49070127,17.526473 C4.18655139,17.3464765 4,17.0193034 4,16.6658832 L4,9.67471899 Z M20,9.56911707 L20,16.6658832 C20,17.0193034 19.8134486,17.3464765 19.5092987,17.526473 L12.8888889,21.4444444 L12.8888889,13.6728275 C12.9050191,13.6647696 12.9210067,13.6561758 12.9368301,13.6470401 L20,9.56911707 Z" fill="#000000"/>
        <path d="M4.21611835,7.74669402 C4.30015839,7.64056877 4.40623188,7.55087574 4.5299008,7.48500698 L11.5299008,3.75665466 C11.8237589,3.60013944 12.1762411,3.60013944 12.4700992,3.75665466 L19.4700992,7.48500698 C19.5654307,7.53578262 19.6503066,7.60071528 19.7226939,7.67641889 L12.0479413,12.1074394 C11.9974761,12.1365754 11.9509488,12.1699127 11.9085461,12.2067543 C11.8661433,12.1699127 11.819616,12.1365754 11.7691509,12.1074394 L4.21611835,7.74669402 Z" fill="#000000" opacity="0.3"/>
    </g>
</svg>
                                                    <!--end::Svg Icon-->
												</span>
                            <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $order->products->count() }}</span>
                            <span class="font-weight-bold text-muted font-size-sm">Produits</span>
                        </div>
                        <!--end::Body-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h3 class="card-title">Commande&nbsp;
                <span class="label label-inline label-xl font-weight-bold label-secondary mr-3">{{ $order->reference }}</span>
                <span class="label label-inline label-xl font-weight-bold label-secondary">N° {{ $order->id }}</span>
            </h3>
        </div>
        <div class="card-body">
            <div class="card card-custom gutter-b">
                <div class="card-header"><h4 class="card-title">Paiement</h4></div>
                <div class="card-body">
                    <p>{{ $order->payment_text }}</p>
                    @if($order->payment_infos)
                        ID de paiement : <span class="label label-inline label-lg label-secondary">{{ $order->payment_infos->payment_id }}</span>
                    @endif
                </div>
            </div>
            @if($shop->invoice && ($order->invoice_id || $order->state->indice > $annule_indice))
                <div class="card card-custom gutter-b">
                    <div class="card-header"><h4 class="card-title">Facture</h4></div>
                    <div class="card-body">
                        @if(session('invoice'))
                            <div class="alert alert-danger" role="alert">
                                {{ session('invoice') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        @if($order->invoice_id)
                           <p>La facture a été générée avec l'id <strong>{{ $order->invoice_id }}</strong> et le numéro <strong>{{ $order->invoice_number }}</strong>.</p>
                        @else
                            <form method="POST" action="{{ route('orders.invoice', $order->id) }}">
                                @csrf
                                <div class="form-group">
                                    <label class="checkbox">
                                        <input type="checkbox" name="paid" @if($order->state->indice > 3) checked @endif>
                                        <span></span>&nbsp;
                                        Le Paiement à été effectuer
                                    </label>
                                </div>
                                <button type="submit" class="btn btn-outline-primary mt-5"><i class="fa fa-file-pdf"></i> Générer la facture</button>
                            </form>
                        @endif
                    </div>
                </div>
            @endif
            @if($order->payment === 'mandat')
                <div class="card card-custom gutter-b">
                    <div class="card-header"><h4 class="card-title">Bon de commande</h4></div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('orders.updateNumber', $order->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="purchase_order">N° du bon de commande</label>
                                <input type="text" class="form-control" name="purchase_order" value="{{ $order->purchase_order }}">
                            </div>
                            <button type="submit" class="btn btn-outline-primary" @if($order->state->indice >= 3) disabled @endif>Mettre à jour le numéro de bon de commande</button>
                        </form>
                    </div>
                </div>
            @endif
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <h4 class="card-title">Etat:&nbsp; <span class="label label-inline label-lg label-{{ $order->state->color }}">{{ $order->state->name }}</span></h4>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('orders.update', $order->id) }}">
                        @csrf
                        @method("PUT")
                        <div class="form-group">

                            <select id="state_id" class="form-control selectpicker" name="state_id">
                                @switch($order->state->indice)
                                    @case(1)
                                        <option data-slug="cheque" value="1" @if($order->state->id === 1) selected @endif>Attente Chèque</option>
                                        <option data-slug="mandat" value="2" @if($order->state->id === 2) selected @endif>Attente mandat administratif</option>
                                        <option data-slug="virement" value="3" @if($order->state->id === 3) selected @endif>Attente Virement</option>
                                        <option data-slug="carte" value="4" @if($order->state->id === 4) selected @endif>Attente paiement par carte</option>
                                        <option data-slug="erreur" value="5" @if($order->state->id === 5) selected @endif>Erreur de paiement</option>
                                        <option data-slug="annule" value="6" @if($order->state->id === 6) selected @endif>Annulée</option>
                                        <option data-slug="mandat_ok" value="7" @if($order->state->id === 7) selected @endif>Mandat Administratif reçu</option>
                                        <option data-slug="paiement_ok" value="8" @if($order->state->id === 8) selected @endif>Paiement reçu</option>
                                        <option data-slug="expedie" value="9" @if($order->state->id === 9) selected @endif>Expédié</option>
                                        <option data-slug="rembourse" value="10" @if($order->state->id === 10) selected @endif>Remboursé</option>
                                    @break
                                    @case(2)
                                        @break
                                    @case(3)
                                    <option data-slug="annule" value="6" @if($order->state->id === 6) selected @endif>Annulée</option>
                                    <option data-slug="paiement_ok" value="8" @if($order->state->id === 8) selected @endif>Paiement reçu</option>
                                    <option data-slug="expedie" value="9" @if($order->state->id === 9) selected @endif>Expédié</option>
                                    <option data-slug="rembourse" value="10" @if($order->state->id === 10) selected @endif>Remboursé</option>
                                    @break
                                    @case(4)
                                    <option data-slug="annule" value="6" @if($order->state->id === 6) selected @endif>Annulée</option>
                                    <option data-slug="expedie" value="9" @if($order->state->id === 9) selected @endif>Expédié</option>
                                    <option data-slug="rembourse" value="10" @if($order->state->id === 10) selected @endif>Remboursé</option>
                                    @break
                                    @case(5)
                                    <option data-slug="annule" value="6" @if($order->state->id === 6) selected @endif>Annulée</option>
                                    <option data-slug="rembourse" value="10" @if($order->state->id === 10) selected @endif>Remboursé</option>
                                    @break
                                    @case(6)
                                    @break
                                @endswitch
                            </select>
                        </div>
                        <button type="submit" class="btn btn-outline-primary">Mettre à jour l'état</button>
                    </form>
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header">
                    <h4 class="card-title">Produits</h4>
                </div>
                <div class="card-body">
                    @foreach ($order->products as $item)
                        <br>
                        <div class="row">
                            <div class="col m6 s12">
                                {{ $item->name }} ({{ $item->quantity }} @if($item->quantity > 1) exemplaires) @else exemplaire) @endif
                            </div>
                            <div class="col m6 s12"><strong>{{ number_format($item->total_price_gross, 2, ',', ' ') }} €</strong></div>
                        </div>
                    @endforeach
                        <hr><br>
                        <div class="row" style="background-color: lightgrey">
                            <div class="col s6">
                                Total HT
                            </div>
                            <div class="col s6">
                                <strong>{{ number_format($order->ht, 2, ',', ' ') }} €</strong>
                            </div>
                        </div>
                        <br>
                        <div class="row" style="background-color: lightgrey">
                            <div class="col s6">
                                Livraison en Colissimo
                            </div>
                            <div class="col s6">
                                <strong>{{ number_format($order->shipping, 2, ',', ' ') }} €</strong>
                            </div>
                        </div>
                        <br>
                        @if($order->tax > 0)
                            <div class="row" style="background-color: lightgrey">
                                <div class="col s6">
                                    TVA à {{ $order->tax * 100 }} %
                                </div>
                                <div class="col s6">
                                    <strong>{{ number_format($order->tva, 2, ',', ' ') }} €</strong>
                                </div>
                            </div>
                            <br>
                        @endif
                        <div class="row" style="background-color: lightgrey">
                            <div class="col s6">
                                Total TTC
                            </div>
                            <div class="col s6">
                                <strong>{{ number_format($order->totalOrder, 2, ',', ' ') }} €</strong>
                            </div>
                        </div>
                </div>
            </div>
            <div class="card card-custom gutter-b">
                <div class="card-header"><h4 class="card-title">Livraison</h4></div>
                <div class="card-body">
                    @if($order->pick)
                        <div class="symbol mr-3">
                            <span class="symbol-label"><i class="fa fa-truck-pickup icon-lg"></i></span>
                        </div>
                        <span class="font-weight-bold font-size-h5">Le client a choisi de venir chercher sa commande sur place</span>
                    @else
                        <div class="symbol symbol-30 mr-3">
                            <img src="/images/expedition/colissimo.png" class="img-fluid" width="30">
                        </div>
                        <span class="font-weight-bold font-size-h5">Livraison par transporteur</span>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h4 class="card-title">
                Client:&nbsp; <a href="{{ route('client.customers.show', $order->user->id) }}"><span class="label label-inline label-xl label-primary mr-3">{{ $order->user->firstname. ' ' .$order->user->name }}</span></a>
                <span class="label label-inline label-xl label-secondary">N° {{ $order->user->id }}</span>
            </h4>
        </div>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">Email</dt>
                <dd class="col-sm-9"><a href="mailto:{{ $order->user->email }}">{{ $order->user->email }}</a></dd>
                <dt class="col-sm-3 text-truncate">Date d'inscription</dt>
                <dd class="col-sm-9">{{ $order->user->created_at->calendar() }} ({{ $order->user->created_at->diffForHumans() }})</dd>
                <dt class="col-sm-3 text-truncate">Commandes validées</dt>
                <dd class="col-sm-9">
                    <span class="label label-inline label-rounded label-primary">{{ $order->user->orders->where('state_id', '>', 5)->count() }}</span>
                </dd>
            </dl>
        </div>
    </div>

    <div class="card card-custom gutter-b">
        <div class="card-header">
            <h3 class="card-title">Adresses</h3>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs nav-tabs-line">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#home"><i class="fa fa-truck"></i>&nbsp; Adresse d'expédition</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#profile"><i class="fa fa-euro-sign"></i>&nbsp; Adresse de facturation</a>
                </li>
            </ul>
            <div class="tab-content mt-5" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                    @if($order->adresses->count() === 1)
                        @include("account.addresses.partials.address", ["address" => $order->adresses->first()])
                    @else
                        @include("account.addresses.partials.address", ["address" => $order->adresses->get(1)])
                    @endif
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="kt_tab_pane_2">
                    @include("account.addresses.partials.address", ["address" => $order->adresses->first()])
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")

@endsection
