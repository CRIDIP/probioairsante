@extends("back.layouts.app")

@section("style")
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => $nameStack,
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => $nameStack,
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => false,
    "btns" => [

    ]
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">{{ $nameStack }}</h3>
            <div class="card-toolbar">
                    @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'config.pays.index')
                        <a href="{{ route('config.pays.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Nouveau pays</a>
                    @endif
                    @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'config.etats.index')
                        <a href="{{ route('config.etats.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle etat</a>
                    @endif
                    @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'config.pages.index')
                        <a href="{{ route('config.pages.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle page</a>
                    @endif
                    @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'catalog.product.index')
                        <a href="{{ route('catalog.product.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Nouveau produit</a>
                    @endif
                    @if(\Illuminate\Support\Facades\Route::currentRouteName() === 'catalog.category.index')
                        <a href="{{ route('catalog.category.create') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Nouvelle catégorie</a>
                    @endif
            </div>
        </div>
        <div class="card-body">
            {{ $dataTable->table(['class' => 'table table-bordered table-hover table-sm'], true) }}
        </div>
    </div>
@endsection

@section("script")
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    {{ $dataTable->scripts() }}
@endsection
