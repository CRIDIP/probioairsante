@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Création d'une catégorie",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Catalogue",
            "url" => null,
            "last" => false
        ],
        [
            "name" => "Catégorie",
            "url" => route('catalog.category.index'),
            "last" => false
        ],
        [
            "name" => "Création d'une catégorie",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Création d'une catégorie</h3>
        </div>
        <div class="card-body">
            <form id="formAddCategory" class="form" action="{{ route('catalog.category.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Nom de la catégorie</label>
                    <input type="text" id="name" class="form-control" name="name">
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormAddCategory" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        function submitFormAddCategory() {
            $("#formAddCategory").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formAddCategory");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormAddCategory');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            submitFormAddCategory();
        });
    </script>
@endsection
