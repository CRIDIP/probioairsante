@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Création d'un produit",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Catalogue",
            "url" => null,
            "last" => false
        ],
        [
            "name" => "Produit",
            "url" => route('catalog.product.index'),
            "last" => false
        ],
        [
            "name" => "Création d'une page",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Création d'un produit</h3>
        </div>
        <div class="card-body">
            <form id="formAddProduct" class="form" action="{{ route('catalog.product.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="category_id">Catégorie</label>
                    <select id="category_id" class="form-control selectpicker" data-live-search="true" name="category_id">
                        <option value="">Selectionnez une catégorie</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Nom du produit</label>
                    <input type="text" id="name" class="form-control" name="name">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="summernote" id="kt_summernote_1" name="description"></textarea>
                </div>

                <div class="form-group">
                    <label for="weight">Poids en Kg</label>
                    <input type="text" id="weight" class="form-control" name="weight">
                </div>

                <div class="form-group">
                    <label for="price">Prix TTC</label>
                    <input type="text" id="price" class="form-control" name="price">
                </div>

                <div class="form-group">
                    <label for="quantity">Quantité actuel</label>
                    <input type="number" id="quantity" class="form-control" name="quantity">
                </div>

                <div class="form-group">
                    <label for="quantity_alert">Quantité pour alerte de stock</label>
                    <input type="number" id="quantity_alert" class="form-control" name="quantity_alert">
                </div>

                <div class="form-group {{ $errors->has('image') ? ' is-invalid' : '' }}">
                    <label for="image">Image</label>
                    @if(isset($product) && !$errors->has('image'))
                        <div>
                            <p><img src="{{ asset('images/thumbs/' . $product->image) }}"></p>
                            <button id="changeImage" class="btn btn-warning">Changer d'image</button>
                        </div>
                    @endif
                    <div id="wrapper">
                        @if(!isset($product) || $errors->has('image'))
                            <div class="custom-file">
                                <input type="file" id="image" name="image"
                                       class="{{ $errors->has('image') ? ' is-invalid ' : '' }}custom-file-input"
                                       required>
                                <label class="custom-file-label" for="image"></label>
                                @if ($errors->has('image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" id="active" checked="checked" name="active">
                        <span></span>&nbsp; Produit Actif</label>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormAddProduct" class="btn btn-success"><i
                            class="fa fa-check-circle"></i> Valider
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTCkeditor = function () {
            // Private functions
            let demos = function () {
                $('.summernote').summernote({
                    height: 150
                });
            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        // Initialization
        jQuery(document).ready(function () {
            $('form').on('change', '#image', e => {
                $(e.currentTarget).next('.custom-file-label').text(e.target.files[0].name);
            });
            $('#changeImage').click(e => {
                $(e.currentTarget).parent().hide();
                $('#wrapper').html(`
          <div id="image" class="custom-file">
            <input type="file" id="image" name="image" class="custom-file-input" required>
            <label class="custom-file-label" for="image"></label>
          </div>`
                );
            });
            KTCkeditor.init()
        });
    </script>
@endsection
