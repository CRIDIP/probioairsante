@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Edition d'une page",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Page",
            "url" => route('config.pages.index'),
            "last" => false
        ],
        [
            "name" => "Edition d'une page",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Edition d'une page</h3>
        </div>
        <div class="card-body">
            <form id="formEditProduct" class="form" action="{{ route('catalog.product.update', $product->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <label for="category_id">Catégorie</label>
                    <select id="category_id" class="form-control selectpicker" data-live-search="true" name="category_id">
                        <option value="{{ $product->category->id }}">{{ $product->category->name }}</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="name">Nom du produit</label>
                    <input type="text" id="name" class="form-control" name="name" value="{{ $product->name }}">
                </div>

                <div class="form-group">
                    <label for="description">Description</label>
                    <textarea class="summernote" id="kt_summernote_1" name="description">{{ $product->name }}</textarea>
                </div>

                <div class="form-group">
                    <label for="weight">Poids en Kg</label>
                    <input type="text" id="weight" class="form-control" name="weight" value="{{ $product->weight }}">
                </div>

                <div class="form-group">
                    <label for="price">Prix TTC</label>
                    <input type="text" id="price" class="form-control" name="price" value="{{ $product->price }}">
                </div>

                <div class="form-group">
                    <label for="quantity">Quantité actuel</label>
                    <input type="number" id="quantity" class="form-control" name="quantity" value="{{ $product->quantity }}">
                </div>

                <div class="form-group">
                    <label for="quantity_alert">Quantité pour alerte de stock</label>
                    <input type="number" id="quantity_alert" class="form-control" name="quantity_alert" value="{{ $product->quantity_alert }}">
                </div>

                <div class="form-group {{ $errors->has('image') ? ' is-invalid' : '' }}">
                    <label for="image">Image</label>
                    @if(isset($product) && !$errors->has('image'))
                        <div>
                            <p><img src="{{ asset('images/thumbs/' . $product->image) }}"></p>
                            <button id="changeImage" class="btn btn-warning">Changer d'image</button>
                        </div>
                    @endif
                    <div id="wrapper">
                        @if(!isset($product) || $errors->has('image'))
                            <div class="custom-file">
                                <input type="file" id="image" name="image"
                                       class="{{ $errors->has('image') ? ' is-invalid ' : '' }}custom-file-input"
                                       required>
                                <label class="custom-file-label" for="image"></label>
                                @if ($errors->has('image'))
                                    <div class="invalid-feedback">
                                        {{ $errors->first('image') }}
                                    </div>
                                @endif
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" id="active" @if($product->active == 1) checked @endif name="active">
                        <span></span>&nbsp; Produit Actif</label>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormAddProduct" class="btn btn-success"><i
                            class="fa fa-check-circle"></i> Valider
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTCkeditor = function () {
            // Private functions
            let demos = function () {
                $('.summernote').summernote({
                    height: 150
                });
            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        function submitFormEditPage() {
            $("#formEditPage").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditPage");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditPage');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            KTCkeditor.init();
            submitFormEditPage();
        });
    </script>
@endsection
