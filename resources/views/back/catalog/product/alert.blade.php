@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Confirmation de la suppression du produit",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Catalogue",
            "url" => null,
            "last" => false
        ],
        [
            "name" => "Produit",
            "url" => route('catalog.product.index'),
            "last" => false
        ],
        [
            "name" => "Confirmation de la suppression du produit",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div id="delProduct" data-id="{{ $product->id }}" data-name="{{ $product->name }}"></div>
@endsection

@section("script")
    <script type="text/javascript">

        // Initialization
        jQuery(document).ready(function () {
            let info = $("#delProduct")
            let ids = info.attr('data-id')
            let name = info.attr('data-name')
            swal.fire({
                title: 'Etes-vous sur ?',
                html: "Vous allez supprimer le produit <strong>" + name + "</strong>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: "Oui, supprimer ce produit",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch('/admin/catalog/product/'+ids, {
                        method: "DELETE",
                        headers: {
                            "Content-Type": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-Token": document.head.querySelector("[name=csrf-token][content]").content
                        },
                    }).then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }

                    }).catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        position: "top-right",
                        icon: "success",
                        title: "Le produit à été supprimer",
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        window.location.href='/admin/catalog/product'
                    });
                }
            })
        });
    </script>
@endsection
