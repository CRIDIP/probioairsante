@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Confirmation de la suppression de l'état de commande",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Pays",
            "url" => route('config.pays.index'),
            "last" => false
        ],
        [
            "name" => "Confirmation de la suppression de l'état de commande",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div id="delState" data-id="{{ $state->id }}" data-name="{{ $state->name }}"></div>
@endsection

@section("script")
    <script type="text/javascript">

        // Initialization
        jQuery(document).ready(function () {
            let info = $("#delState")
            let ids = info.attr('data-id')
            let name = info.attr('data-name')
            swal.fire({
                title: 'Etes-vous sur ?',
                html: "Vous allez supprimer l'état <strong>" + name + "</strong>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: "Oui, supprimer cette état",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch('/admin/configuration/etats/'+ids, {
                        method: "DELETE",
                        headers: {
                            "Content-Type": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-Token": document.head.querySelector("[name=csrf-token][content]").content
                        },
                    }).then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }

                    }).catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        position: "top-right",
                        icon: "success",
                        title: "L'état à été supprimer",
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        window.location.href='/admin/configuration/etats'
                    });
                }
            })
        });
    </script>
@endsection
