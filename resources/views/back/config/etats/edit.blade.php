@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Edition d'un état de commande",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Etat de commande",
            "url" => route('config.pays.index'),
            "last" => false
        ],
        [
            "name" => "Edition d'un état de commande",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Edition d'un état de commande</h3>
        </div>
        <div class="card-body">
            <form id="formEditStateOrder" class="form" action="{{ route('config.etats.update', $state->id) }}" method="POST">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <label for="name">Nom de l'état</label>
                    <input type="text" id="name" class="form-control" name="name" value="{{ $state->name }}">
                </div>

                <div class="form-group">
                    <label for="slug">Slug de l'état</label>
                    <input type="text" id="slug" class="form-control" name="slug" value="{{ $state->slug }}">
                </div>

                <div class="form-group">
                    <label for="indice">Indice de l'état</label>
                    <input type="number" id="indice" class="form-control" name="indice" value="{{ $state->indice }}">
                </div>

                <div class="form-group">
                    <label for="color">Couleur de l'état</label>
                    <select id="color" name="color" class="form-control selectpicker">
                        <option value="">Selectionner une couleur</option>
                        <option value="primary" data-content="<span class='label label-rounded label-primary'>&nbsp;</span> Primary" @if($state->color === 'primary') selected @endif></option>
                        <option value="warning" data-content="<span class='label label-rounded label-warning'>&nbsp;</span> Warning" @if($state->color === 'warning') selected @endif></option>
                        <option value="danger" data-content="<span class='label label-rounded label-danger'>&nbsp;</span> Danger" @if($state->color === 'danger') selected @endif></option>
                        <option value="success" data-content="<span class='label label-rounded label-success'>&nbsp;</span> Success" @if($state->color === 'success') selected @endif></option>
                    </select>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormEditStateOrder" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        function submitFormEditStateOrder() {
            $("#formEditStateOrder").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditStateOrder");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditStateOrder');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            submitFormEditStateOrder();
        });
    </script>
@endsection
