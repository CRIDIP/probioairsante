@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Edition d'une page",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Page",
            "url" => route('config.pages.index'),
            "last" => false
        ],
        [
            "name" => "Edition d'une page",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Edition d'une page</h3>
        </div>
        <div class="card-body">
            <form id="formEditPage" class="form" action="{{ route('config.pages.update', $page->id) }}" method="POST">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <label for="title">Titre</label>
                    <input type="text" id="title" class="form-control" name="title" value="{{ $page->title }}">
                </div>

                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" id="slug" class="form-control" name="slug" value="{{ $page->slug }}">
                </div>

                <div class="form-group">
                    <label for="text">Contenue de la Page</label>
                    <textarea class="summernote" id="kt_summernote_1" name="text">{{ $page->text }}</textarea>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormEditPage" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTCkeditor = function () {
            // Private functions
            let demos = function () {
                $('.summernote').summernote({
                    height: 150
                });
            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        function submitFormEditPage() {
            $("#formEditPage").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditPage");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditPage');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            KTCkeditor.init();
            submitFormEditPage();
        });
    </script>
@endsection
