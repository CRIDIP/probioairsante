@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Création d'un pays",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Pays",
            "url" => route('config.pays.index'),
            "last" => false
        ],
        [
            "name" => "Création d'un pays",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Création d'un pays</h3>
        </div>
        <div class="card-body">
            <form id="formAddPays" class="form" action="{{ route('config.pays.store') }}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="name">Nom du pays</label>
                    <input type="text" id="name" class="form-control" name="name">
                </div>
                <div class="form-group">
                    <label for="tax">Taxe ou TVA</label>
                    <input type="text" id="tax" class="form-control" name="tax">
                </div>
                <div class="text-center">
                    <button type="submit" id="btnSubmitFormAddPays" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        function submitFormAddPays() {
            $("#formAddPays").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formAddPays");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormAddPays');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            submitFormAddPays();
        });
    </script>
@endsection
