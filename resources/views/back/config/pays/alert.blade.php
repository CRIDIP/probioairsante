@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Confirmation de la suppression du pays",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Pays",
            "url" => route('config.pays.index'),
            "last" => false
        ],
        [
            "name" => "Confirmation de la suppression du pays",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div id="delPays" data-id="{{ $country->id }}" data-name="{{ $country->name }}"></div>
@endsection

@section("script")
    <script type="text/javascript">

        // Initialization
        jQuery(document).ready(function () {
            let info = $("#delPays")
            let ids = info.attr('data-id')
            let name = info.attr('data-name')
            swal.fire({
                title: 'Etes-vous sur ?',
                html: "Vous allez supprimer le pays <strong>" + name + "</strong>",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: "Oui, supprimer ce pays",
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return fetch('/admin/configuration/pays/'+ids, {
                        method: "DELETE",
                        headers: {
                            "Content-Type": "application/json",
                            "X-Requested-With": "XMLHttpRequest",
                            "X-CSRF-Token": document.head.querySelector("[name=csrf-token][content]").content
                        },
                    }).then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }

                    }).catch(error => {
                        Swal.showValidationMessage(
                            `Request failed: ${error}`
                        )
                    })
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    Swal.fire({
                        position: "top-right",
                        icon: "success",
                        title: "Le pays à été supprimer",
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        window.location.href='/admin/configuration/pays'
                    });
                }
            })
        });
    </script>
@endsection
