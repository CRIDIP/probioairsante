@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Edition d'un pays: ".$country->name,
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Pays",
            "url" => route('config.pays.index'),
            "last" => false
        ],
        [
            "name" => "Edition d'un pays",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Edition d'un pays</h3>
        </div>
        <div class="card-body">
            <form id="formEditPays" class="form" action="{{ route('config.pays.update', $country) }}" method="POST">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <label for="name">Nom du pays</label>
                    <input type="text" id="name" class="form-control" name="name" value="{{ $country->name }}">
                </div>
                <div class="form-group">
                    <label for="tax">Taxe ou TVA</label>
                    <input type="text" id="tax" class="form-control" name="tax" value="{{ $country->tax }}">
                </div>
                <div class="text-center">
                    <button type="submit" id="btnSubmitFormEditPays" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        function submitFormEditPays() {
            $("#formEditPays").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditPays");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditPays');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "PUT",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            submitFormEditPays()
        });
    </script>
@endsection
