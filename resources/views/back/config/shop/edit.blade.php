@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Configuration de la boutique",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Configuration de la boutique",
            "url" => route('config.boutique'),
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <form id="formEditBoutique" class="form" action="{{ route('config.boutique.update') }}" method="POST">
        @csrf
        @method("PUT")
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h3 class="card-title">Identité</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Nom de la boutique</label>
                    <input type="text" id="name" class="form-control" name="name" value="{{ $shop->name }}"/>
                </div>
                <div class="form-group">
                    <label for="address">Adresse de la boutique</label>
                    <input type="text" id="address" class="form-control" name="address" value="{{ $shop->address }}"/>
                </div>
                <div class="form-group">
                    <label for="email">Adresse Mail de la boutique</label>
                    <input type="email" id="email" class="form-control" name="email" value="{{ $shop->email }}"/>
                </div>
                <div class="form-group">
                    <label for="phone">Numéro de téléphone de la boutique</label>
                    <input type="tel" id="phone" class="form-control" name="phone" value="{{ $shop->phone }}"/>
                </div>
                <div class="form-group">
                    <label for="facebook">URL Facebook de la boutique</label>
                    <input type="text" id="facebook" class="form-control" name="facebook"
                           value="{{ $shop->facebook }}"/>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h3 class="card-title">Banque</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="holder">Titulaire</label>
                    <input type="text" id="holder" class="form-control" name="holder" value="{{ $shop->holder }}"/>
                </div>
                <div class="form-group">
                    <label for="bank">Nom de la banque</label>
                    <input type="text" id="bank" class="form-control" name="bank" value="{{ $shop->bank }}"/>
                </div>
                <div class="form-group">
                    <label for="bank_address">Adresse de la banque</label>
                    <input type="text" id="bank_address" class="form-control" name="bank_address"
                           value="{{ $shop->bank_address }}"/>
                </div>
                <div class="form-group">
                    <label for="bic">BIC</label>
                    <input type="text" id="bic" class="form-control" name="bic" value="{{ $shop->bic }}"/>
                </div>
                <div class="form-group">
                    <label for="iban">IBAN</label>
                    <input type="text" id="iban" class="form-control" name="iban" value="{{ $shop->iban }}"/>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h3 class="card-title">Acceuil</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label for="home">Titre</label>
                    <input type="text" id="home" class="form-control" name="home" value="{{ $shop->home }}"/>
                </div>
                <div class="form-group">
                    <label for="home_infos">Informations Générales</label>
                    <textarea id="home_infos" name="home_infos" class="tox-target">
                        {!! $shop->home_infos !!}
                    </textarea>
                </div>
                <div class="form-group">
                    <label for="home_shipping">Frais d'expéditions</label>
                    <textarea id="home_shipping" name="home_shipping" class="tox-target">
                        {!! $shop->home_shipping !!}
                    </textarea>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h3 class="card-title">Facturation</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="checkbox checkbox-lg">
                        <input type="checkbox" @if($shop->invoice == true) checked @endif name="invoice">
                        <span></span>
                        &nbsp;Activation de la facturation
                    </label>
                </div>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <h3 class="card-title">Mode de paiement accepter</h3>
            </div>
            <div class="card-body">
                <div class="form-group">
                    <label class="checkbox checkbox-lg">
                        <input type="checkbox" @if($shop->card == true) checked @endif name="card">
                        <span></span>
                        &nbsp;Carte bancaire
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox checkbox-lg">
                        <input type="checkbox" @if($shop->transfer == true) checked @endif name="transfer">
                        <span></span>
                        &nbsp;Virement Bancaire
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox checkbox-lg">
                        <input type="checkbox" @if($shop->check == true) checked @endif name="check">
                        <span></span>
                        &nbsp;Chèque
                    </label>
                </div>
                <div class="form-group">
                    <label class="checkbox checkbox-lg">
                        <input type="checkbox" @if($shop->mandat == true) checked @endif name="mandat">
                        <span></span>
                        &nbsp;mandat Administratif
                    </label>
                </div>
                <button type="submit" id="btnSubmitFormEditBoutique" class="btn btn-block btn-success"><i
                        class="fa fa-check-circle"></i> Valider
                </button>
            </div>
        </div>
    </form>
@endsection

@section("script")
    <script src="/assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js"></script>
    <script type="text/javascript">
        let KTCkeditor = function () {
            // Private functions
            let demos = function () {
                ClassicEditor
                    .create(document.querySelector('#home_infos'))
                    .then(editor => {
                        console.log(editor);
                    })
                    .catch(error => {
                        console.error(error);
                    });
            };

            let demos2 = function () {
                ClassicEditor
                    .create(document.querySelector('#home_shipping'))
                    .then(editor => {
                        console.log(editor);
                    })
                    .catch(error => {
                        console.error(error);
                    });
            };

            return {
                // public functions
                init: function () {
                    demos();
                    demos2();
                }
            };
        }();

        function submitEditFormBoutique() {
            $("#formEditBoutique").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditBoutique");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditBoutique');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner', 'Veuillez patienter...');

                $.ajax({
                    url: url,
                    method: "PUT",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages)
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            KTCkeditor.init();
            submitEditFormBoutique();
        });
    </script>
@endsection
