@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Edition d'une page",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Configuration",
            "url" => route('config.index'),
            "last" => false
        ],
        [
            "name" => "Maintenance",
            "url" => null,
            "last" => false
        ],
        [
            "name" => "Maintenance du site",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => false,
    "btns" => []
])
@endsection

@section("content")
    <div class="card card-custom mb-5">
        <div class="card-header">
            <h3 class="card-title">Edition de l'interface de maintenance</h3>
        </div>
        <div class="card-body">
            <form id="formEditMaintenance" class="form" action="{{ route('config.maintenance.update') }}" method="POST">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <label for="ip">Adresse IP autorisée</label>
                    <input type="text" id="ip" class="form-control" name="ip" value="{{ $ip }}">
                </div>

                <div class="form-group">
                    <label for="message">Message à afficher</label>
                    <input type="text" id="message" class="form-control" name="message" value="{{ $message }}">
                </div>

                <div class="form-group">
                    <span class="switch">
                        <label>
                            <input type="checkbox" name="active" @if($active) checked @endif/>
                            <span></span> &nbsp; Mode Maintenance
                        </label>
                    </span>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormEditPage" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Cache</h3>
        </div>
        <div class="card-body">
            <form id="formEditMaintenance" class="form" action="{{ route('config.maintenance.cache') }}" method="POST">
                @csrf
                @method("PUT")
                <div class="form-group">
                    <span class="switch">
                        <label>
                            <input type="checkbox" name="config" @if($config) checked @endif/>
                            <span></span> &nbsp; Cache de la configuration
                        </label>
                    </span>
                </div>

                <div class="form-group">
                    <span class="switch">
                        <label>
                            <input type="checkbox" name="route" @if($route) checked @endif/>
                            <span></span> &nbsp; Cache des routes
                        </label>
                    </span>
                </div>

                <div class="text-center">
                    <button type="submit" id="btnSubmitFormEditPage" class="btn btn-success"><i class="fa fa-check-circle"></i> Valider</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let KTCkeditor = function () {
            // Private functions
            let demos = function () {
                $('.summernote').summernote({
                    height: 150
                });
            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        function submitFormEditPage() {
            $("#formEditPage").on('submit', (e) => {
                e.preventDefault();
                let form = $("#formEditPage");
                let url = form.attr('action');
                let btn = KTUtil.getById('btnSubmitFormEditPage');
                let data = form.serializeArray();

                KTUtil.btnWait(btn, 'spinner spinner-right spinner-white pr-15', 'Veuillez patienter');

                $.ajax({
                    url: url,
                    method: "POST",
                    data: data,
                    statusCode: {
                        200: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.success(data)
                        },
                        419: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error("Erreur de jeton: TOKEN incomplet")
                        },
                        422: (data) => {
                            KTUtil.btnRelease(btn);
                            toastr.warning(data.messages);
                            console.log(data.errors)
                        },
                        500: (jqxhr) => {
                            KTUtil.btnRelease(btn);
                            toastr.error(jqxhr.responseText)
                        }
                    },
                })
            })
        }

        // Initialization
        jQuery(document).ready(function () {
            KTCkeditor.init();
            submitFormEditPage();
        });
    </script>
@endsection
