@if(session()->has('success'))
    <div class="alert alert-custom alert-notice alert-light-success fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-checking"></i>
        </div>
        <div class="alert-text">{{ session('success') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('warning'))
    <div class="alert alert-custom alert-notice alert-light-warning fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-warning"></i>
        </div>
        <div class="alert-text">{{ session('warning') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('error'))
    <div class="alert alert-custom alert-notice alert-light-danger fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-cross"></i>
        </div>
        <div class="alert-text">{{ session('error') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
            </button>
        </div>
    </div>
@endif

@if(session()->has('info'))
    <div class="alert alert-custom alert-notice alert-light-primary fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon2-information"></i>
        </div>
        <div class="alert-text">{{ session('info') }}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
            </button>
        </div>
    </div>
@endif
