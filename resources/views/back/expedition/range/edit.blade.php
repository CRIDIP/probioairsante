@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Gestion des plages de poids",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Expédition",
            "url" => route('exp.index'),
            "last" => false
        ],
        [
            "name" => "Gestion des plages de poids",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
    <div class="alert alert-custom alert-notice alert-light-warning fade show mb-5" role="alert">
        <div class="alert-icon">
            <i class="flaticon-warning"></i>
        </div>
        <div class="alert-text">
            Si vous supprimez une plage les valeurs correspondantes dans les tarifs des expéditions par pays seront aussi supprimées. Il est vivement conseillé d'effecteur ces modifications en mode maintenance !
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
																	<span aria-hidden="true">
																		<i class="ki ki-close"></i>
																	</span>
            </button>
        </div>
    </div>
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Plages</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('exp.range.update') }}" method="POST">
                @csrf
                @method("PUT")
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <th></th>
                            <th>Poids Maximum</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($ranges as $range)
                            <tr>
                                <td>Plage {{ $loop->iteration }}</td>
                                <td>
                                    <input type="text" name="{{ $loop->iteration }}" class="form-control max" value="{{ $range->max }}">
                                </td>
                                <td>
                                    @if($loop->last)
                                    <button type="button" class="btn btn-danger btn-block"><i class="fa fa-trash"></i> Supprimer</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Enregistrer</button>
                    <button type="button" class="btn btn-primary float-right"><i class="fa fa-plus-circle"></i> Ajouter une plage</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        let ranges = [];
        const checkValue = (indice, value) => {
            if(isNaN(value)) {
                return false;
            }
            if(indice) {
                if(value < ranges[indice - 1]) {
                    return false;
                }
            }
            if(indice < ranges.length) {
                if(value > ranges[indice + 1]) {
                    return false;
                }
            }
            return true;
        }
        $(document).ready(() => {
            $('.max').each((index, element) => {
                ranges.push(Number($(element).val()))
            });
            $(document).on('change', 'input', e => {
                const indice = $(e.currentTarget).attr('name') - 1;
                const value = $(e.currentTarget).val();
                if(checkValue(indice, value)) {
                    ranges[indice] = value;
                } else {
                    $('input[name=' + (indice + 1) + ']').val(ranges[indice]);
                    $(e.currentTarget).removeClass('is-invalid');
                    $('button[type=submit]').removeClass('disabled');
                }
            });
            $(document).on('input', 'input', e => {
                const indice = $(e.currentTarget).attr('name') - 1;
                const input = $('input[name=' + (indice + 1) + ']');
                if(checkValue(indice, $(e.currentTarget).val())) {
                    input.removeClass('is-invalid');
                    $('button[type=submit]').removeClass('disabled');
                } else {
                    input.addClass('is-invalid');
                    $('button[type=submit]').addClass('disabled');
                }
            });
            $(document).on('click', 'button.btn-danger', e => {
                $('input[name=' + (ranges.length) + ']').parent().parent().remove();
                ranges.pop();
                if(ranges.length) {
                    $('input[name=' + (ranges.length) + ']').parent().next().html(
                        '<button type="button" class="btn btn-danger btn-block">Supprimer</button>'
                    );
                }
            });
            $('button.btn-primary').click(e => {
                e.preventDefault();
                $('input[name=' + (ranges.length) + ']').parent().next().html('');
                ranges.push(Number(ranges[ranges.length - 1]) + 1);
                const html = `
        <tr>
          <td>Plage ${ranges.length}</td>
          <td><input name="${ranges.length}" type="text" class="form-control max" value="${ranges[ranges.length - 1]}"></td>
          <td><button type="button" class="btn btn-danger btn-block">Supprimer</button></td>
        </tr>
        `;
                $('tbody').append(html);
            });

        });
    </script>
@endsection
