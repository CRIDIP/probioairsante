@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Gestion des tarifs postaux",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Expédition",
            "url" => route('exp.index'),
            "last" => false
        ],
        [
            "name" => "Gestion des tarifs postaux",
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => false,
    "btns" => []
])
@endsection

@section("content")
    @if($errors-> isNotEmpty())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            Il y a des erreurs dans les valeurs, veuillez corriger les entrées signalées en rouge.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif
    <div class="card card-custom">
        <div class="card-header">
            <h3 class="card-title">Tarifs des envois en Colissimo par pays et plage de poids</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('exp.colissimo.update') }}" method="POST">
                @csrf
                @method("PUT")
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Pays</th>
                                @foreach($ranges as $range)
                                <th><= {{ $range->max }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($countries as $country)
                                <tr>
                                    <td>{{ $country->name }}</td>
                                    @foreach($country->ranges as $range)
                                    <td>
                                        <input type="text" class="form-control {{ $errors->has('n' . $range->pivot->id) ? ' is-invalid' : '' }}" name="n{{ $range->pivot->id }}" value="{{ old('n' . $range->pivot->id, $range->pivot->price) }}" required>
                                    </td>
                                    @endforeach
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check-circle"></i> Enregistrer</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
