@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Information sur le client :".\App\Helpers\Generator::firsLetter($user->firstname).".".$user->name,
    "breads" => [
        [
            "name" => "Accueil",
            "url" => route('admin'),
            "last" => false
        ],
        [
            "name" => "Client",
            "url" => route('client.customers.index'),
            "last" => false
        ],
        [
            "name" => "Information sur le client :".\App\Helpers\Generator::firsLetter($user->firstname).".".$user->name,
            "url" => null,
            "last" => true
        ]
    ],
    "returnControl" => true,
    "btns" => []
])
@endsection

@section("content")
<div class="row">
    <div class="col-md-6">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon"><i class="la la-user-circle-o"></i> </span>
                    <h3 class="card-label">{{ $user->name }} {{ $user->firstname }} [{{ $user->id }}] - <a href="mailto:{{ $user->email }}">{{ $user->email }}</a></h3>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <td class="text-right">Date d'inscription</td>
                        <td class="text-left">{{ $user->created_at->format('d/m/Y à H:i') }} ({{ $user->created_at->diffForHumans() }})</td>
                    </tr>
                    <tr>
                        <td class="text-right">Dernière connexion</td>
                        <td class="text-left">{{ $user->last_seen->format('d/m/Y à H:i') }} ({{ $user->last_seen->diffForHumans() }})</td>
                    </tr>
                    <tr>
                        <td class="text-right">Inscription</td>
                        <td class="text-left">
                            @if($user->newsletter == 1)
                                <span class="label label-inline label-xl label-success"><i class="fa fa-check text-white"></i>&nbsp; Lettre d'information</span>
                            @else
                                <span class="label label-inline label-xl label-danger"><i class="fa fa-times text-white"></i>&nbsp; Lettre d'information</span>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td class="text-right">Dernière mise à jour</td>
                        <td class="text-left">{{ $user->updated_at->format('d/m/Y à H:i') }} ({{ $user->updated_at->diffForHumans() }})</td>
                    </tr>
                    <tr>
                        <td class="text-right">Etat</td>
                        <td class="text-left">
                            @if($user->active == 1)
                                <span class="label label-inline label-xl label-success"><i class="fa fa-check text-white"></i>&nbsp; Actif</span>
                            @else
                                <span class="label label-inline label-xl label-danger"><i class="fa fa-times text-white"></i>&nbsp; Inactif</span>
                            @endif
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon"><i class="la la-shopping-basket"></i> </span>
                    <h3 class="card-label">Commandes <span class="label label-primary">{{ $user->orders->count() }}</span> </h3>
                </div>
            </div>
            <div class="card-body">
                <div class="card card-custom mb-10">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <strong>Commandes Validée:</strong> <span class="label label-success">{{ $countCommandeValid }}</span>
                                pour un montant de {{ \App\Helpers\Generator::formatCurrency($sumCommandValid) }}.
                            </div>
                            <div class="col-md-6">
                                <strong>Commandes Non Validée:</strong> <span class="label label-danger">{{ $countCommandeInvalid }}</span>
                                pour un montant de {{ \App\Helpers\Generator::formatCurrency($sumCommandInvalid) }}.
                            </div>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Paiement</th>
                        <th>Etat</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->orders as $order)
                        <tr>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->created_at->calendar() }}</td>
                            <td>{{ $order->payment_text }}</td>
                            <td><span class="label label-inline label-{{ $order->state->color }}">{{ $order->state->name }}</span> </td>
                            <td>{{ \App\Helpers\Generator::formatCurrency($order->total) }}</td>
                            <td>
                                <a href="{{ route('orders.show', $order->id) }}" class="btn btn-icon btn-sm btn-primary"><i class="fa fa-eye"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-custom gutter-b">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon"><i class="la la-clock-o"></i> </span>
                    <h3 class="card-label">Dernières Connexions</h3>
                </div>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Ip</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($user->connexions as $connexion)
                        <tr>
                            <td>{{ $connexion->id }}</td>
                            <td>{{ $connexion->created_at->format('d/m/Y à H:i') }}</td>
                            <td>{{ $connexion->ip }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
    <div class="card card-custom">
        <div class="card-header">
            <div class="card-title">
                <span class="card-icon"><i class="la la-map-pin"></i> </span>
                <h3 class="card-label">Liste des adresses</h3>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Société</th>
                            <th>Nom</th>
                            <th>Adresse</th>
                            <th>Pays</th>
                            <th>Téléphone</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($user->addresses as $address)
                        <tr>
                            <td>
                                @if($address->company) {{ $address->company }} @else (Néant) @endif
                            </td>
                            <td>{{ $address->civility }} {{ $address->name }} {{ $address->firstname }}</td>
                            <td>
                                {{ $address->address }}<br>
                                @if($address->addressbis) {{ $address->addressbis }} <br>@endif
                                @if($address->bp) {{ $address->bp }} <br>@endif
                                {{ $address->postal }} {{ $address->city }}
                            </td>
                            <td>{{ $address->country->name }}</td>
                            <td>{{ $address->phone }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        // Initialization
        jQuery(document).ready(function () {

        });
    </script>
@endsection
