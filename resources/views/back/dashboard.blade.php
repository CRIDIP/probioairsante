@extends("back.layouts.app")

@section("style")

@endsection

@section("subheader")
    @include("back.layouts.partials._subheader", [
    "active" => true,
    "titlePage" => "Tableau de Bord",
    "breads" => [
        [
            "name" => "Accueil",
            "url" => "/",
            "last" => false
        ],
        [
            "name" => "Tableau de Bord",
            "url" => "/",
            "last" => true
        ]
    ],
    "returnControl" => false,
    "btns" => [
        /*[
            "name" => "Editer",
            "url" => "/",
            "class" => "btn btn-primary",
            "icon" => "fa fa-edit"
        ]*/
    ]
])
@endsection

@section("content")

@endsection

@section("script")

@endsection
