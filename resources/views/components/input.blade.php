<div class="form-group">
    <label for="{{ $name ?? '' }}">{{ $label }}</label>
    <input type="{{ $type }}" id="{{ $name ?? '' }}" name="{{ $name ?? '' }}" value="{{ old($name ?? '', $value !== '' ? $value : '') }}" class="{{ $errors->has($name ?? '') ? 'is-invalid' : '' }}" {{ $required ? 'required' : '' }}>
    <div class="invalid-feedback">{{ $errors->has($name ?? '') ? $errors->first($name ?? '') : '' }}</div>
</div>
