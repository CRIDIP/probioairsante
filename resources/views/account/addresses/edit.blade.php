@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Mettre à jour mon adresse</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

                <a href="{{ route('account.adresses.index') }}" class="btn btn-sm btn-outline-dark mr-3"><i class="la la-arrow-circle-left"></i>Retour </a>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        @include("components.alert")
        <div class="card card-custom">
            <form action="{{ route('account.adresses.update', $adress->id) }}" id="formCreateAddress" method="POST">
                @method("PUT")
            @include('account.addresses.partials.form')
            </form>
        </div>
    </div>
@endsection

@section("script")
    @include('account.addresses.partials.script')
    <script type="text/javascript">

    </script>
@endsection
