@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Mes Adresses</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <a href="{{ route('account') }}" class="btn btn-sm btn-outline-dark mr-3"><i class="la la-arrow-circle-left"></i>Retour </a>
                <a href="{{ route('account.adresses.store') }}" class="btn btn-sm btn-outline-info"><i class="la la-plus-circle"></i>Nouvelle adresse </a>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        @include("components.alert")
        <div class="row">
            @foreach($addresses as $address)
                <div class="col-md-4">
                    <div class="card card-custom">
                        <div class="card-body">
                            @include("account.addresses.partials.address")
                        </div>
                        <div class="card-footer text-right">
                            <a href="{{ route('account.adresses.edit', $address->id) }}" class="btn btn-outline-primary btn-icon"><i class="fa fa-edit"></i></a>
                            <a href="{{ route('account.adresses.destroy', $address->id) }}" class="btn btn-outline-danger btn-icon delete"><i class="fa fa-trash"></i></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">

    </script>
@endsection
