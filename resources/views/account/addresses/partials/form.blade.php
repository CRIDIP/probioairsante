<div class="card-body">
    @csrf
    <div class="form-group">
        <label class="checkbox">
            <input type="checkbox" name="professionnal"
                   id="professionnal" {{ old('professionnal', isset($adress) ? $adress->professionnal : false) ? 'checked' : '' }}>
            <span></span>
            &nbsp;C'est une adresse professionnelle
        </label>
    </div>
    <div class="form-group">
        <div class="radio-inline">
            <label class="radio">
                <input type="radio" name="civility"
                       value="Mme" {{ old('civility', isset($adress) ? $adress->civility : '') == 'Mme' ? 'checked' : '' }}>
                <span></span>&nbsp; Mme.</label>
            <label class="radio">
                <input type="radio" name="civility"
                       value="M." {{ old('civility', isset($adress) ? $adress->civility : '') == 'M.' ? 'checked' : '' }}>
                <span></span>&nbsp; M.</label>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Nom</label>
        <input type="text" class="form-control" name="name" value="{{ isset($adress) ? $adress->name : '' }}">
    </div>

    <div class="form-group">
        <label for="firstname">Prénom</label>
        <input type="text" class="form-control" name="firstname" value="{{ isset($adress) ? $adress->firstname : '' }}">
    </div>

    <div class="form-group">
        <label for="company">Raison Social</label>
        <input type="text" class="form-control" name="company" value="{{ isset($adress) ? $adress->company : '' }}">
    </div>

    <div class="form-group">
        <label for="address">Adresse</label>
        <input type="text" class="form-control" name="address" value="{{ isset($adress) ? $adress->address : '' }}">
    </div>

    <div class="form-group">
        <label for="addressbis">Complément d'adresse (Batiment,Immeuble,etc...)</label>
        <input type="text" class="form-control" name="addressbis"
               value="{{ isset($adress) ? $adress->addressbis : '' }}">
    </div>

    <div class="form-group">
        <label for="bp">Lieu-dit ou BP</label>
        <input type="text" class="form-control" name="bp" value="{{ isset($adress) ? $adress->bp : '' }}">
    </div>

    <div class="form-group">
        <label for="postal">Code Postal</label>
        <input type="text" class="form-control" name="postal" value="{{ isset($adress) ? $adress->postal : '' }}">
    </div>

    <div class="form-group">
        <label for="city">Ville</label>
        <input type="text" class="form-control" name="city" value="{{ isset($adress) ? $adress->city : '' }}">
    </div>

    <div class="form-group">
        <label for="country">Pays</label>
        <select class="form-control selectpicker" data-live-search="true" name="country_id">
            <option value=""></option>
            @foreach($countries as $country)
                <option value="{{ $country->id }}"
                        @if(old('country_id', isset($adress) ? $adress->country_id : '') == $country->id) selected @endif>{{ $country->name }}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="phone">Téléphone</label>
        <input type="text" class="form-control" name="phone" value="{{ isset($adress) ? $adress->phone : '' }}">
    </div>

    <div class="text-center">
        <button type="submit" id="btnSubmitFormCreateAddress" class="btn btn-success"><i class="fa fa-check"></i>
            Valider
        </button>
    </div>
</div>
