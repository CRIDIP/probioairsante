@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">RGPD</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-body">
                <div class="accordion accordion-light accordion-toggle-arrow" id="accordionExample5">
                    <div class="card">
                        <div class="card-header" id="headingOne5">
                            <div class="card-title" data-toggle="collapse" data-target="#access">
                                <i class="flaticon-information"></i> Accès à mes informations</div>
                        </div>
                        <div id="access" class="collapse" data-parent="#accordionExample5">
                            <div class="card-body">
                                {{ config('message.rgpd') }}
                                <a href="{{ route('account.rgpd.pdf') }}" class="btn btn-block btn-primary"><i class="fa fa-file-pdf"></i> Récupérer mes informations</a>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header" id="headingOne5">
                            <div class="card-title" data-toggle="collapse" data-target="#error">
                                <i class="flaticon-edit"></i> Rectification des erreurs</div>
                        </div>
                        <div id="error" class="collapse" data-parent="#accordionExample5">
                            <div class="card-body text-center align-middle">
                                Vous pouvez modifier toutes les informations personnelles accessibles depuis la page de votre compte : identité, adresses. Pour toute autre rectification contactez nous en nous envoyant un e-mail à cette adresse : {{ $email }}. Nous vous répondrons dans les plus brefs délais.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
