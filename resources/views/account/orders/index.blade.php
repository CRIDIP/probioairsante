@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Mes commandes</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <a href="{{ route('account') }}" class="btn btn-sm btn-outline-dark mr-3"><i class="la la-arrow-circle-left"></i>Retour </a>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="card card-custom">
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">Liste de mes commandes</span>
                </h3>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Num de commande</th>
                                <th>Date</th>
                                <th>Total</th>
                                <th>Paiement</th>
                                <th>Etat</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr class="font-size-h6-lg">
                                <td>{{ $order->reference }}</td>
                                <td>{{ $order->created_at->calendar() }}</td>
                                <td>{{ \App\Helpers\Generator::formatCurrency($order->total_order) }}</td>
                                <td>{{ $order->payment_text }}</td>
                                <td><span class="label label-inline label-{{ $order->state->color }}">{{ $order->state->name }}</span></td>
                                <td>
                                    <a href="{{ route('account.command.show', $order->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i> Détails</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
