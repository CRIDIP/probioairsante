@extends('layouts.app')

@section("style")
    <style>
        .StripeElement {
            box-sizing: border-box;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }
        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }
        .StripeElement--invalid {
            border-color: #fa755a;
        }
        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
        .loader {
            margin: auto;
            border: 16px solid #e3e3e3;
            border-radius: 50%;
            border-top: 16px solid #1565c0;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Commande N°{{ $order->reference }}</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">
                <a href="{{ route('account.command.index') }}" class="btn btn-sm btn-outline-dark mr-3"><i class="la la-arrow-circle-left"></i>Retour </a>
            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fa fa-calendar-check fa-2x text-info"></i>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $order->created_at->calendar() }}</span>
                        <span class="font-weight-bold text-muted font-size-sm">Date</span>
                    </div>
                    <!--end::Body-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fa fa-money-bill fa-2x text-success"></i>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ \App\Helpers\Generator::formatCurrency($order->total) }}</span>
                        <span class="font-weight-bold text-muted font-size-sm">Montant total</span>
                    </div>
                    <!--end::Body-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="card card-custom bgi-no-repeat card-stretch gutter-b" style="background-position: right top; background-size: 30% auto; background-image: url(/assets/media/svg/shapes/abstract-1.svg)">
                    <!--begin::Body-->
                    <div class="card-body">
                        <i class="fa fa-boxes fa-2x text-primary"></i>
                        <span class="card-title font-weight-bolder text-dark-75 font-size-h2 mb-0 mt-6 d-block">{{ $order->products()->count() }}</span>
                        <span class="font-weight-bold text-muted font-size-sm">Nombre de produit</span>
                    </div>
                    <!--end::Body-->
                </div>
            </div>
        </div>
        <div class="card card-custom mb-3">
            <div class="card-body">
                @include('command.partials.detail', [
                                    'tax' => $order->tax,
                                    'shipping' => $order->shipping,
                                    'total' => $order->total,
                                    'content' => $order->products,
                                  ])
            </div>
        </div>
        <div class="card card-custom mb-3">
            <div class="card-body">
                <strong>Moyen de paiement:</strong> {{ $order->payment_text }}<br>
                <strong>Etat:</strong> {{ $order->state->name }}<br>
                @if($order->state->slug === 'carte' || $order->state->slug === 'erreur')
                    @include('command.partials.stripe')
                @endif
                <div id="payment_ok">
                    <div class="card card-custom">
                        <div class="card-header border-0 bg-success">
                            <div class="card-title">
								<span class="card-icon">
									<i class="flaticon2-check-mark text-white"></i>
								</span>
                                <h3 class="card-label text-white">Votre paiement à été valider</h3>
                            </div>
                        </div>
                        <div class="separator separator-solid separator-white opacity-20"></div>
                        <div class="card-body text-white">
                            @if($order->pick)
                                <p>Vous pouvez venir chercher votre commande</p>
                            @else
                                <p>Votre commande va vous être envoyée</p>
                            @endif
                            <p>Pour toute question ou information complémentaire merci de contacter notre support client.</p>
                        </div>
                    </div>
                </div>
                @if($order->invoice_id)
                    <br>
                    <td><a href="#" class="btn btn-sm btn-primary">Télécharger la facture</a></td>
                @endif
            </div>
        </div>
    </div>
@endsection

@section("script")
    @include('command.partials.stripejs')
    <script type="text/javascript">
        (function ($) {

        })(jQuery)
    </script>
@endsection
