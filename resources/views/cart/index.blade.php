@extends('layouts.app')

@section("style")

@endsection

@section('subheader')
    <div class="subheader py-2 py-lg-6  subheader-transparent " id="kt_subheader">
        <div class=" container  d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">

                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">Mon Panier</h5>
                    <!--end::Page Title-->

                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->

            <!--begin::Toolbar-->
            <div class="d-flex align-items-center">

            </div>
            <!--end::Toolbar-->
        </div>
    </div>
@endsection

@section('content')
    <!--begin::Container-->
    <div class=" container ">
        <div class="card card-custom gutter-b" id="cardCart">
            <!--begin::Header-->
            <div class="card-header flex-wrap border-0 pt-6 pb-0">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder font-size-h3 text-dark">Mon panier</span>
                </h3>
                <div class="card-toolbar">
                    <div class="dropdown dropdown-inline">
                        <a href="#" class="btn btn-primary font-weight-bolder font-size-sm">
                            Continuer mes achats
                        </a>
                    </div>
                </div>
            </div>
            <!--end::Header-->

            <div class="card-body">
                <!--begin::Shopping Cart-->
                <div class="table-responsive" id="shoppingCart">
                    <table class="table">
                        <!--begin::Cart Header-->
                        <thead>
                        <tr>
                            <th>Produit</th>
                            <th class="text-center">Qte</th>
                            <th class="text-right">Montant</th>
                            <th></th>
                        </tr>
                        </thead>
                        <!--end::Cart Header-->

                        <tbody>
                        <!--begin::Cart Content-->
                        @if(!Cart::isEmpty())
                            @foreach($carts as $cart)
                                <tr>
                                    <td class="d-flex align-items-center font-weight-bolder">
                                        <!--begin::Symbol-->
                                        <div class="symbol symbol-60 flex-shrink-0 mr-4 bg-light">
                                            <div class="symbol-label" style="background-image: url('/images/thumbs/{{ $cart['associatedModel']['image'] }}')"></div>
                                        </div>
                                        <!--end::Symbol-->
                                        <a href="#" class="text-dark text-hover-primary">{{ $cart['name'] }}</a>
                                    </td>
                                    <td class="text-center align-middle">
                                        <span class="mr-2 font-weight-bolder">{{ $cart['quantity'] }}</span>
                                    </td>
                                    <td class="text-right align-middle font-weight-bolder font-size-h5">
                                        {{ \App\Helpers\Generator::formatCurrency($cart['price']*$cart['quantity']) }}
                                    </td>
                                    <td class="text-right align-middle">
                                        <a href="{{ route('panier.destroy', $cart['id']) }}" class="btn btn-icon btn-danger deleteItem"><i class="fa fa-trash"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            <!--begin::Cart Footer-->
                            <tr>
                                <td colspan="2"></td>
                                <td class="font-weight-bolder font-size-h4 text-right">Sous Total</td>
                                <td class="font-weight-bolder font-size-h4 text-right">{{ \App\Helpers\Generator::formatCurrency(Cart::getSubTotal()) }}</td>
                            </tr>
                            <tr>
                                <td colspan="4" class="border-0 text-muted text-right pt-0">Hors Frais de port. TVA Incluse</td>
                            </tr>
                            <tr>
                                <td colspan="2" class="border-0 pt-10">
                                    &nbsp;
                                </td>
                                <td colspan="2" class="border-0 text-right pt-10">
                                    <a href="{{ route('commandes.create') }}" class="btn btn-success font-weight-bolder px-8"><i class="fa fa-check"></i> Commander </a>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td colspan="4" class="text-center font-weight-bold font-size-h5">Le panier est vide</td>
                            </tr>
                        @endif
                        <!--end::Cart Content-->
                        <!--end::Cart Footer-->
                        </tbody>
                    </table>
                </div>
                <!--end::Shopping Cart-->
            </div>
        </div>
    </div>
    <!--end::Container-->
@endsection

@section("script")
    <script type="text/javascript">
        let divCart = KTUtil.getById('cardCart');
        $(".deleteItem").on('click', (e) => {
            e.preventDefault();
            let a = $(this);
            let url = a.attr('href');
            $.ajax({
                url: url,
                method: "GET",
            }).done((data) => {
                window.location.reload()
            }).fail((jqxhr) => {
                toastr.error(jqxhr.responseText)
            })
        })
    </script>
@endsection
