<?php
use Illuminate\Support\Facades\Route;

Route::group(["prefix" => "admin", "middleware" => ["admin"], "namespace" => "Back"], function (){
    Route::get('/', ["as" => "admin", "uses" => "AdminController@index"]);

    //Route::resource('product', 'ProductController')->except('show');

    Route::group(["prefix" => "client"], function (){
       Route::group(["prefix" => "customers"], function (){
           Route::get('', ["as" => "client.customers.index", "uses" => "UserController@index"]);
           Route::get('{user_id}', ["as" => "client.customers.show", "uses" => "UserController@show"]);
       });
    });

    Route::group(["prefix" => "catalog"], function (){
        Route::group(["prefix" => "category"], function (){
            Route::get('', ["as" => "catalog.category.index", "uses" => "ProductCategoryController@index"]);
            Route::get('create', ["as" => "catalog.category.create", "uses" => "ProductCategoryController@create"]);
            Route::post('create', ["as" => "catalog.category.store", "uses" => "ProductCategoryController@store"]);
            Route::get('{category}', ["as" => "catalog.category.edit", "uses" => "ProductCategoryController@edit"]);
            Route::put('{category}', ["as" => "catalog.category.update", "uses" => "ProductCategoryController@update"]);
            Route::get('{category}/confirm', ["as" => "catalog.category.alert", "uses" => "ProductCategoryController@alert"]);
            Route::delete('{category}', ["as" => "catalog.category.destroy", "uses" => "ProductCategoryController@destroy"]);
        });

        Route::group(["prefix" => "product"], function (){
            Route::get('', ["as" => "catalog.product.index", "uses" => "ProductController@index"]);
            Route::get('create', ["as" => "catalog.product.create", "uses" => "ProductController@create"]);
            Route::post('create', ["as" => "catalog.product.store", "uses" => "ProductController@store"]);
            Route::get('{product}', ["as" => "catalog.product.edit", "uses" => "ProductController@edit"]);
            Route::put('{product}', ["as" => "catalog.product.update", "uses" => "ProductController@update"]);
            Route::get('{product}/confirm', ["as" => "catalog.product.alert", "uses" => "ProductController@alert"]);
            Route::delete('{product}', ["as" => "catalog.product.destroy", "uses" => "ProductController@destroy"]);
        });
    });

    Route::group(["prefix" => "orders"], function (){
        Route::get('', ["as" => "orders.index", "uses" => "OrderController@index"]);
        Route::get('{order_id}', ["as" => "orders.show", "uses" => "OrderController@show"]);
        Route::put('{order_id}', ["as" => "orders.update", "uses" => "OrderController@update"]);
        Route::post('{order_id}/invoice', ["as" => "orders.invoice", "uses" => "OrderController@invoice"]);
        Route::put('{order_id}/number', ["as" => "orders.updateNumber", "uses" => "OrderController@updateNumber"]);
    });

    Route::group(["prefix" => "configuration"], function (){
        Route::get('/', ["as" => "config.index", "uses" => "ConfigController@index"]);
        Route::get('boutique', ["as" => "config.boutique", "uses" => "ShopController@edit"]);
        Route::put('boutique', ["as" => "config.boutique.update", "uses" => "ShopController@update"]);

        Route::group(["prefix" => "pays"], function (){
            Route::get('/', ["as" => "config.pays.index", "uses" => "CountryController@index"]);
            Route::get('create', ["as" => "config.pays.create", "uses" => "CountryController@create"]);
            Route::post('create', ["as" => "config.pays.store", "uses" => "CountryController@store"]);
            Route::get('{pays}', ["as" => "config.pays.edit", "uses" => "CountryController@edit"]);
            Route::put('{pays}', ["as" => "config.pays.update", "uses" => "CountryController@update"]);
            Route::get('{pays}/confirm', ["as" => "config.pays.alert", "uses" => "CountryController@alert"]);
            Route::delete('{pays}', ["as" => "config.pays.destroy", "uses" => "CountryController@destroy"]);
        });

        Route::group(["prefix" => "etats"], function (){
            Route::get('/', ["as" => "config.etats.index", "uses" => "StateController@index"]);
            Route::get('create', ["as" => "config.etats.create", "uses" => "StateController@create"]);
            Route::post('create', ["as" => "config.etats.store", "uses" => "StateController@store"]);
            Route::get('{state_id}', ["as" => "config.etats.edit", "uses" => "StateController@edit"]);
            Route::put('{state_id}', ["as" => "config.etats.update", "uses" => "StateController@update"]);
            Route::get('{state_id}/confirm', ["as" => "config.etats.alert", "uses" => "StateController@alert"]);
            Route::delete('{state_id}', ["as" => "config.etats.destroy", "uses" => "StateController@destroy"]);
        });

        Route::group(["prefix" => "pages"], function (){
            Route::get('/', ["as" => "config.pages.index", "uses" => "PageController@index"]);
            Route::get('create', ["as" => "config.pages.create", "uses" => "PageController@create"]);
            Route::post('create', ["as" => "config.pages.store", "uses" => "PageController@store"]);
            Route::get('{state_id}', ["as" => "config.pages.edit", "uses" => "PageController@edit"]);
            Route::put('{state_id}', ["as" => "config.pages.update", "uses" => "PageController@update"]);
            Route::get('{state_id}/confirm', ["as" => "config.pages.alert", "uses" => "PageController@alert"]);
            Route::delete('{state_id}', ["as" => "config.pages.destroy", "uses" => "PageController@destroy"]);
        });

        Route::group(["prefix" => "maintenance"], function (){
            Route::get('/', ["as" => "config.maintenance.edit", "uses" => "MaintenanceController@edit"]);
            Route::put('/', ["as" => "config.maintenance.update", "uses" => "MaintenanceController@update"]);
            Route::put('cache', ["as" => "config.maintenance.cache", "uses" => "MaintenanceController@cache"]);
        });

    });
    Route::group(["prefix" => "expedition"], function (){
        Route::get('/', ["as" => "exp.index"]);

        Route::group(['prefix' => "range"], function (){
            Route::get('edit', ["as" => "exp.range.edit", "uses" => "RangeController@edit"]);
            Route::put('edit', ["as" => "exp.range.update", "uses" => "RangeController@update"]);
        });

        Route::group(["prefix" => "tarif"], function (){
            Route::get('edit', ["as" => "exp.colissimo.edit", "uses" => "ColissimoController@edit"]);
            Route::put('edit', ["as" => "exp.colissimo.update", "uses" => "ColissimoController@update"]);
        });
    });
});
