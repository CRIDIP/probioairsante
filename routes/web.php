<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
include('auth.php');

Route::get('/', ["as" => "home", "uses" => "HomeController@index"]);
Route::group(["prefix" => "boutique"], function (){
    Route::get('/', ["as" => "produits.index", "uses" => "ProductController@index"]);

    Route::group(["prefix" => "{category_id}"], function () {
        Route::get('/', ["as" => "produits.category", "uses" => "ProductController@category"]);
        Route::get('/produits/{produit_id}', ["as" => "produits.show", "uses" => "ProductController@show"]);
    });
});
Route::group(["prefix" => "panier"], function () {
    Route::get('/', ["as" => "panier.index", "uses" => "CartController@index"]);
    Route::post('/', ["as" => "panier.store", "uses" => "CartController@store"]);
    Route::get('/{id}', ["as" => "panier.destroy", "uses" => "CartController@destroy"]);
});
Route::group(["middleware" => ["auth"], "prefix" => "commandes"], function () {
    Route::get('create', ["as" => "commandes.create", "uses" => "OrderController@create"]);
    Route::post('/', ["as" => "commandes.store", "uses" => "OrderController@store"]);
    Route::get('confirmation/{order}', ["as" => "commandes.confirmation", "uses" => "OrdersController@confirmation"]);
    Route::post('payment/{order}', ["as" => "commandes.payment", "uses" => "PaymentController@payment"]);
});

Route::group(["middleware" => ["auth"], "prefix" => "account"], function () {
    Route::get('/', ["as" => "account", "uses" => "AccountController@index"]);
    Route::get('identite', ["as" => "account.identite.edit", "uses" => "IdentiteController@edit"]);
    Route::put('identite', ["as" => "account.identite.update", "uses" => "IdentiteController@update"]);
    Route::get('rgpd', ["as" => "account.rgpd", "uses" => "IdentiteController@rgpd"]);
    Route::get('rgpd/pdf', ["as" => "account.rgpd.pdf", "uses" => "IdentiteController@pdf"]);

    Route::group(["prefix" => "adresses"], function () {
        Route::get('/', ["as" => "account.adresses.index", "uses" => "AddressController@index"]);
        Route::get('create', ["as" => "account.adresses.create", "uses" => "AddressController@create"]);
        Route::post('create', ["as" => "account.adresses.store", "uses" => "AddressController@store"]);
        Route::get('{address}', ["as" => "account.adresses.edit", "uses" => "AddressController@edit"]);
        Route::put('{address}', ["as" => "account.adresses.update", "uses" => "AddressController@update"]);
        Route::get('{address}/delete', ["as" => "account.adresses.destroy", "uses" => "AddressController@destroy"]);
    });

    Route::group(["prefix" => "commande"], function () {
        Route::get('/', ["as" => "account.command.index", "uses" => "OrdersController@index"]);
        Route::get('{command_id}', ["as" => "account.command.show", "uses" => "OrdersController@show"]);
    });
});

include('admin.php');

//Auth::routes();
Route::get('page/{page:slug}', 'HomeController@page')->name('page');
Route::get('logout', 'Auth\LoginController@logout');
Route::get('test', 'TestController@test');
